```math
\tag*{Eq. 4.1}
\begin{alignedat}{1}
 &SurvivalRate(Ages 3-5) = (1-.015)^12 = 0.83\\
 &NaturalMortalityRate(Ages 3-5) = 1-SurvivalRate(Ages 3-5) = 0.17\\
 &SurvivalRate(Age 2) = (1-0.03)^12 = 0.69\\
 &NaturalMortalityRate(Age 2) = 1-SurvivalRate(Age 2) = 0.31
\end{alignedat}
```