[Sept 2019](https://gitlab.com/chinook-technical-committee/programs/ream/wikis/Minutes/2019.09.21-PBS)  
[Nov 2019](https://gitlab.com/chinook-technical-committee/programs/ream/wikis/Minutes/2019.11.22-PBS)<br>
[Jan 13 2020](https://gitlab.com/chinook-technical-committee/programs/ream/wikis/Minutes/2020.01.13-Portland)<br>
[Jan 16 2020](https://gitlab.com/chinook-technical-committee/programs/ream/wikis/Minutes/2020.01.16-Portland)<br>
[Feb 4 2020](https://gitlab.com/chinook-technical-committee/programs/ream/-/wikis/Minutes/2020.02.04-PBS)<br>
[May 13 2020](https://gitlab.com/chinook-technical-committee/programs/ream/-/wikis/Minutes/2020.05.13-Remote)<br>
[June 3 2020](https://gitlab.com/chinook-technical-committee/programs/ream/-/wikis/Minutes/2020.06.03-Cyberspace)<br>