
| done [x] incomplete [-]| subroutine | grouping | translator | comment |
|----------------|---------------|---------------|---------------|---------------|
| - [-] | Public Sub PrimarySub                   | PrimarySub | CW |  |
| - [x] | Private Sub cmdOK_Click                 | interface | CW |  |
| - [ ] | Private Sub frmMain_Closed              |  |  |  |
| - [ ] | Public Sub mnuProgramVersion_Click      |  |  |  |
| - [ ] | Public Sub mnuHelpAbout_Popup           |  |  |  |
| - [ ] | Public Sub mnuHelpAbout_Click           |  |  |  |
| - [ ] | Sub addNewFishery                       |  |  |  |
| - [x] | Sub CalcAnnualTotals                    | algorithm | CW | decided to skip -uses variable that are not defined  |
| - [-] | Sub CalcCNR                             | algorithm | CW |  |
| - [-] | Sub CalcCohort                          | algorithm | CW |  |
| - [-] | Sub CalcCohort_IncompleteBrood          | algorithm | CW |  |
| - [-] | Sub CalcExploitRates                    | algorithm | CW |  |
| - [-] | Sub CalcFinalTotals                     | algorithm | CW |  |
| - [-] | Sub CalcLegal                           | algorithm | CW |  |
| - [-] | Sub ShakerMethod1                       | algorithm | CW |  |
| - [ ] | Sub ShakerMethod4                       | algorithm | CW | decided to skip - uses new stock specific PNV values |
| - [-] | Sub ReadMeanLengthSD                    | readin | CW | decided to skip |
| - [ ] | Sub LISTFILES(                          |  |  |  |
| - [-] | Sub ReadMinVulnLen                      | readin | CW | decided to skip - method not tested or currently used |
| - [-] | Sub ReadSizeLimits                      | readin | CW | decided to skip - method not tested or currently used |
| - [-] | Sub CreatePNV                           | algorithm | CW | decided to skip - method not tesed or currently used |
| - [-] | Sub CheckCompleteBY                     | CheckFish | CW |  |
| - [-] | Sub CheckFishNames                     | CheckFish | CW |  |
| - [-] | Sub CheckNumAges                       | CheckFish |  |  |
| - [x] | Sub CheckNumFish                       | CheckFish |  |  |
| - [-] | Sub CNRNoDir                           | algorithm  | CW |  |
| - [-] | Sub CombineFish                         | algorithm | CW |  |
| - [-] | Sub FindTerminal                        | CheckFish |  |  |
| - [x] | Sub readCMBspreadsheet                  | readin | CW, MF |  |
| - [x] | Sub GETPNVDATA                          | readin | CW |  |
| - [-] | Sub readCM1file                         | readin | CW |  |
| - [ ] | Public Sub GetAccessProvider(           |  |  |  |
| - [x] | Sub openCASdb(                          | readin | CW |  |
| - [ ] | Sub readCfilesDB(                       | readin | CW |  |
| - [-] | Sub readCfiles(                         | readin | CW | eratools::read_Cfiles. not sure if it fulfills needs. do we make it a dependent or port the function?  |
| - [x] | Sub CreateTagCodeList                  | readin | CW |  |
| - [-] | Sub readCDSfile                        | readin | CW | eratools::readCDSfile, not sure if it fulfills needs |
| - [x] | Sub PopulateReleaseTablebyBYfromDB      | readin | CW |  |
| - [-] | Sub readsetofQs                        | readin | CW | decided to skip  |
| - [ ] | Sub OutputAnalysis(                     | output |  |  |
| - [ ] | Sub PrintBL(                            | output |  |  |
| - [ ] | Sub PrintBlkFin(                        | output |  |  |
| - [ ] | Sub PrintBlock1(                        | output |  |  |
| - [ ] | Sub PrintBlock2(                        | output |  |  |
| - [ ] | Sub PrintBlock3(                        | output |  |  |
| - [ ] | Sub PrintDistLine(                      | output |  |  |
| - [ ] | Sub PrintError(                         |  |  |  |
| - [ ] | Sub PrintHeader(                        | output |  |  |
| - [ ] | Sub PrintNum(                           |  |  |  |
| - [ ] | Sub Print_OUT(                          | output | MF| writeOUTfileBY & writeOUTfileBYrollup |
| - [ ] | Sub PrintRightLabel(                    | output |  | |
| - [ ] | Sub PrintleftLabel(                     | output |  | |
| - [x] | Sub ProductionWeight(                   | algorithm |  CW | |
| - [ ] | Sub ResetCatches(                       | output |  |  |
| - [ ] | Sub print_THR(                          | output |  |  |
| - [ ] | Sub print_SVR(                          | output |  |  |
| - [ ] | Sub Print_HRJ(                          | output |  | something from ctctools |
| - [ ] | Sub Print_IM(                           | output |  |  |
| - [ ] | Sub print_D_OUT(                        | output |  |  |
| - [ ] | Sub print_OcnHR(                        | output |  |  |
| - [ ] | Sub SaveStkFile(                        |  |  |  |
| - [ ] | Sub SetBY(                              |  |  |  |
| - [-] | Sub SumYearCat                        | algorithm |  |  |
| - [x] | Sub TestBY                             | CheckFish | CW |  |
| - [ ] | Sub TestTot(                            | CheckFish |  |  |
| - [ ] | Sub FileDialog(                         |  |  |  |
| - [ ] | Sub GetPath(                            |  |  |  |
| - [ ] | Private Sub GeometricMean_Click(        |  |  |  |
| - [ ] | Private Sub ArithmeticMean_Click(       |  |  |  |
| - [ ] | Private Sub useIncompBroodChkBox_Click  |  |  |  |
| - [ ] | Sub ERA_Output_ByStock(                 |  |  |  |
| - [ ] | Sub ERA_Output_ByStockAndFishery(       |  |  |  |
| - [x] | Sub Print_CFILES(                       | writeout | CW | produces ERA_Input_CFILEDATA.csv |
| - [ ] | Private Sub RadioButton3_CheckedChanged |  |  |  |
| - [-] | Private Sub StartForm_Load(             | interface | CW | decided to skip  |
| - [-] | Function CalcEstmCohrt(                 | algorithm | CW |  |
| - [-] | Function CalcEstmCohrt2(                | algorithm | CW |  |
| - [ ] | Function ND(                            |  |  |  |
| - [x] | Function ChooseFishType(                |  |  |  |
| - [x] | Function FindBY(                        | CheckFish | CW |  |
| - [ ] | Function FindSubScrpt(                  |  |  |  |
| - [ ] | Function FindTotRel(                    |  |  |  |
| - [ ] | Function executeSQLSingle(              |  |  |  |
| - [ ] | Function executeSQLDataTable(           |  |  |  |
| - [ ] | Function masterCDSList(                 |  |  |  |
| - [ ] | Function MakeFormat(                    |  |  |  |
| - [ ] | Function ReduceAdltEqv(                 |  |  |  |
| - [-] | Function SumTotCat_Age(                 |  |  |  |
| - [-] | Function SumMatAgeCat(                  |  |  |  |
| - [ ] | Function SumAnnCat(                     |  |  |  |
| - [-] | Function sumCat4(                       |  |  |  |
| - [-] | Function sumCat3(                       |  |  |  |
| - [ ] | Function sumCat2(                       |  |  |  |
| - [ ] | Function TestDistribution(              |  |  |  |
| - [ ] | Function TestDistribution2(             |  |  |  |
| - [ ] | Function TestEnd(                       |  |  |  |
| - [x] | readOUTfile_BY                          | readout | MF | R function only |
| - [x] | readOUTfileBYrollup                     | readout | MF | R function only |
| - [x] | writeOUTfile_BY                         | readout | MF | R function only | 
