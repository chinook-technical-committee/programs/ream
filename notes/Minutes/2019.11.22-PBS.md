##  **Location:** 

PBS

## **Attendees:** 
McMillan, Folkes, Komick, Brown, Velez-Espino, Brown (for Fisheries and CAS discussion)

## **Agenda:**

*  Meeting record keeping - Gitlab vs sharepoint (Michael)
*  Approaches to ERA output validation (R vs VB). 
      * Background (MF)
      * Yaml/JSON applications (Nick)
*  Fishery mapping of CWT recoveries (Gayle)
*  CAS bad record table output (Mark/Gayle)
*  Discuss data request from Catherine/UBC Phd student re: distribution tables

## **Discussions**

*  Discussed whether to continue with this wiki for meeting minutes and project discussions. 
     * Agreed that the wiki will be a good platform for "Coders"
----

1. CoShak ERA/REAM Validation
*  We explored format options for REF (REAM Exchange Format)...json was explored and some R export/import [JSON example](https://gitlab.com/chinook-technical-committee/programs/ream/snippets/1915790). 
      *  a row based verbose format works well for json export/import so we will adopt.
      *  we discussed maybe starting with output comparison of .HRJ and .OUT files. 
         * Michael has written a reader for .HRJ and will explore a reader for .OUT files  
         * will need to compare these final outputs eventually to confirm equivalency between CoShak and REAM 
         * will need to develop/define the REF format (variables, multi-dimensional arrays, type)
      *  we also discussed in-process validation that will be required to initially validate REAM.
         *  it was felt we would need this
         *  we will output to REF format 
         *  the breakpoints in the vb code for output we identified are:
            *  after sub FindTerminal
            *  after sub CalcLegal
            *  after sub ShakerMethod (initially first and last pass)
            *  after sub CalcCohort (initially first and last pass)

      *  Demonstration of existing R code used in DFO MRP data validation.
         *  it was felt we could use this to do the JSON validation
         *  can be modified to work with our REF format
----
2.  Framework/Work Plan for ERA output validation
    * [background/evaluation](https://gitlab.com/chinook-technical-committee/programs/ream/wikis/On-Implementing-REAM)  
    
----
3.  Fisheries Mappings
    *   Discussed maintenance of historical groupings and their ongoing Maintenance
    *   Nick will take over from Gayle/Dawn
    *   re-examine fisheries as currently defined in CAS
        *   reconstruct mappings for better accuracy/currency
    *   Gayle recommends adding more ERA fisheries for the DFO at some point in the near future
    *   Gayle and Nick to examine/redefine current fishery lookups
    *   Mark to integrate into the 2020 ERA tables

----
4. CAS loading anomolies
    *   Some records didn't load, Gayle has records.
    *   Mark to get from Gayle, and validate in the load routine test bed.

----
5. Data Request 
    *   Discussed the data request and the source data
        *   Gayle will supply the output files to Mark

## **Action Items**

| Action | Who | Date |
| :--- | :------: |  ---:|
| examine CoShak for REF definition/output | Catarina/Mark  | Feb 2020 |
| .OUT file reader for R | Michael | April 2020 |
| Modify existing R Validation Routines for REF | Nick | May 2020 |
| Redefine DFO Fishery Mappings/Lookups | Nick/Gayle | Dec 2020 |
| Explore loading anomolies from last year | Mark | Jan 2020 |
| Produce Mortality Distribution data for 2017 Appendix C for UBC Student| Gayle/Mark | Dec 2020 |
|  |  |  |

