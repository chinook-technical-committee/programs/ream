##  **Location:** 3:00 PM, Room 113, Embassy Suites Portland 

## **Attendees:** Nick Komak, Michael Folkes, Mark McMillan, David Leonard, Charlie Waters, Gary Morishima, Oliver Miler, Derek Dapp, Catarina Wor

## **Agenda:**
* Review Last Agenda
* pkgdown - Nick
* File reader update - Michael 
* Repository Tour - Michael/Nick
* Implementation Plan - Review/discuss
* Create Issues for current tasks/steps in Gitlab
* Other???

## **Discussions**
1. pkgdown 
   * Nick has done some initial testing
   * Shows potential, can generate equations and webpages from function headers
   * Will require well structured and complete headers
   * Nick to bring in an existing template function header
   
*  Michael has confirmed that his File reader routines are working 

----
2. Overview  
    * For those relatively new we re-visited the overview and plan
    *   

## **Action Items**

| Action | Who | Date |
| :--- | :------: |  ---:|
| Carry On | everyone |  |
|  |  |  |

