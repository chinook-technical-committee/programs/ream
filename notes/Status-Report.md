
[comment]: <> ([[_TOC_]])
___________________________
## Jan 12, 2021

### Where things stand
- The code in REAM almost exactly matches that in coshak. Differences only exist in minor house-keeping tasks (sources of variable names, how to name output files) but all math operations and their naming matches coshak. 
- REAM is now working and produces output for 44 of the 59 ERA stocks.
- Debugging is ongoing and testing of REAM in the 2021 ERA is expected.
- Following our practice of keeping development versions of code in forks/branches separate from the main repository, the most current [development package is here.](https://gitlab.com/MichaelFolkes/ream)
Once it successfully produces output (but perhaps not matching output) for all stocks, it will be merged with the main repository.

### Completed To Date
#### Output files
- *.out (Brood year specific and not BY specific)
- *.hrj text files
This matches the output needs from coshak.
#### Output Validation
- comparison tool export mis-matching results, from .out file tables, to xls file.
- hrj comparison in varied ways: tools in R, or stock text file comparison software (e.g. diffmerge)


### Current Problems
- Of the 59 ERA stocks, REAM crashes for 15 stocks.
  - Useful stock-specific comments contributed by Ethan in table below.
  - A general suggestion from Ethan: 
> and one more thing I would check in the REAM world... are there (at least some) recoveries for each of the codes designated for those stocks that are crashing?  I’ve started spot checking the later year c files and this is a daunting task
- It produces output for the remaining 44, with results matching coshak's to varying degrees (not documented here).


### Coshak Testing

To confirm that for the 15 stocks that REAM cannot process all have complete and correct input files, we wish to rerun coshak for those same 15 stocks.

We request that individuals review stocks below. If a stock that they run in coshak is identified below as 'crashed', then we request that the [Sept 29 zipped ERA input files](https://ctc.psc.org/awg/exploitation-rate-analysis/2020ERA_IN_9-29-2020_4age.zip) be downloaded from sharepoint, the user runs coshak using these files, then report back to the group whether or not coshak successfully ran. This is a test of coshak's response to these particular files as we have already demonstrated one case of incomplete inputs for one stock in this Zip file collection.

|stock |status  |comments                                                                            |user               |coshak.status                                                       |
|:-----|:-------|:-----------------------------------------------------------------------------------|:------------------|:-------------------------------------------------------------------|
|AKS   |        |                                                                                    |                   |                                                                    |
|ATN   |        |                                                                                    |                   |                                                                    |
|ATS   |        |                                                                                    |                   |                                                                    |
|BQR   |        |                                                                                    |                   |                                                                    |
|CHI   |        |                                                                                    |                   |                                                                    |
|CHK   |        |                                                                                    |                   |                                                                    |
|CHM   |crashed |format of the .cds file looks off                                                   |David              |runs ok                                                                    |
|COW   |        |                                                                                    |                   |                                                                    |
|CWF   |        |                                                                                    |                   |                                                                    |
|DOM   |        |                                                                                    |                   |                                                                    |
|ELK   |        |                                                                                    |                   |                                                                    |
|ELW   |        |                                                                                    |                   |                                                                    |
|GAD   |crashed |missing brood years                                                                 |David/Galen        |corruped cmb, runs ok                                               |
|GRN   |crashed |missing brood years                                                                 |Galen              |corruped cmb, runs ok                                               |
|HAN   |        |                                                                                    |                   |                                                                    |
|HAR   |        |                                                                                    |                   |                                                                    |
|HOK   |crashed |                                                                                    |Kris               |successfully run                                                    |
|KLM   |        |                                                                                    |                   |                                                                    |
|KLY   |        |                                                                                    |                   |                                                                    |
|LRH   |        |                                                                                    |                   |                                                                    |
|LRW   |crashed |missing brood years                                                                 |Tim?               |Tommy ran successfully                                              |
|LYF   |crashed |missing brood years; 85 has entry with no codes in the .cds; actually many years do |Tommy              |Tommy ran successfully                                              |
|LYY   |crashed |missing brood years; 85 has entry with no codes in the .cds; actually many years do |Tommy              |Tommy ran successfully                                              |
|MSH   |crashed |BY to 2017; maybe overflow?                                                         |Chuck/Antonio/Nick |                                                                    |
|NAN   |        |                                                                                    |                   |                                                                    |
|NIC   |        |                                                                                    |                   |                                                                    |
|NIS   |crashed |BY to 2018; maybe overflow?                                                         |Galen              |corruped cmb, runs ok                                               |
|NKS   |        |                                                                                    |                   |                                                                    |
|NSA   |        |                                                                                    |                   |                                                                    |
|NSF   |        |                                                                                    |                   |                                                                    |
|PHI   |crashed |BY to 2017; maybe overflow?                                                         |Antonio            |                                                                    |
|PPS   |        |                                                                                    |                   |                                                                    |
|QUE   |crashed |                                                                                    |Galen              |corruped cmb, runs ok                                               |
|QUI   |        |                                                                                    |                   |                                                                    |
|RBT   |        |                                                                                    |                   |                                                                    |
|SAM   |        |                                                                                    |                   |                                                                    |
|SHU   |        |                                                                                    |                   |                                                                    |
|SKF   |        |                                                                                    |                   |                                                                    |
|SKS   |        |                                                                                    |                   |                                                                    |
|SKY   |        |                                                                                    |                   |                                                                    |
|SMK   |        |                                                                                    |                   |                                                                    |
|SOO   |        |                                                                                    |                   |                                                                    |
|SPR   |crashed |                                                                                    |TOmmy              |successfully run                                                    |
|SPS   |crashed |missing brood years                                                                 |Galen              |corruped cmb, runs ok                                               |
|SPY   |        |                                                                                    |                   |                                                                    |
|SQP   |        |                                                                                    |                   |                                                                    |
|SRH   |crashed |missing brood years                                                                 |Tim                |successful after adding missing ccf, & missing a line for central T |
|SSA   |        |                                                                                    |                   |                                                                    |
|SSF   |        |                                                                                    |                   |                                                                    |
|STI   |        |                                                                                    |                   |                                                                    |
|STL   |        |                                                                                    |                   |                                                                    |
|SUM   |        |                                                                                    |                   |                                                                    |
|TAK   |        |                                                                                    |                   |                                                                    |
|TST   |        |                                                                                    |                   |                                                                    |
|UNU   |        |                                                                                    |                   |                                                                    |
|URB   |        |                                                                                    |                   |                                                                    |
|UWA   |        |                                                                                    |                   |                                                                    |
|WRY   |        |                                                                                    |                   |                                                                    |
|WSH   |crashed |                                                                                    |Ethan              |successfully run                                                    |



### To Do
- debug (code or input data files) to identify reasons for crashing on 15 stocks.
- validation for all 59 stocks
- data import from `CAMP` data base (summer 2021).

### What's Needed?
#### export of additional output files.  
- choshak also produces some additional files that are not seemingly an input to other stages: `ocn.srv`, `svr`, `ocn.hr`, `.prn` files 
- does anyone rely on these? (no program appears to depend on them)