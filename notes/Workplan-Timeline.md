## Current Status
- all code to import text files and execute the ERA is complete
- example run times: 3 stocks took 160s, so about 48min to run 54 stocks.
- code to export .out files has been drafted (ie _roughed in_) but not yet complete nor tested
- ream still crashes at several stages for numerous stocks

|    | Fail| Success|
|:---|-----:|----:|
|AK  |     9|    0|
|TBR |     3|    0|
|CA  |     3|   12|
|WA  |     7|    7|
|OR  |    11|    2|

## Workplan
- I have had no available time to work on ream during the last year. Other projects are completing in late June, allowing for my return to this project
- Week of May 24, meeting with Catarina to make a plan for re-starting the completion of debugging.
- Additional support for camp table importing is anticipated from Serena Wong
- As his time allows, Noel will likely be giving support to complete the file export functions (ie .out files and possibly other formats)

| Month | Task |
| ------ | ------ |
| July-Aug| Michael mostly away|
| Sept| Debugging to full functionality with text files for import (ie not anticipating CAMP integration)|
| Sept| Completion of export functions|
| Oct| Preliminary testing with a small group|
| Nov| Michael likely away|
| early Dec| Demonstration with AWG members who have used COSHAK|

## Future Implementation Required
- data import from CAMP

## Future Implementation Aspired
- data export to CAMP (or other dbase)
- SHINY GUI integration