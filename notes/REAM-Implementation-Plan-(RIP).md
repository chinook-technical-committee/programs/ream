# The Goal
Implementation of the chinook exploitation rate analysis in R as a replacement for the current standard "coshak".

## The Objectives
*  Validation: the incremental calculations and output from REAM must be validated against the current standard for the ERA VB code (coshak).
* R package development
* GUI implementation
* Documentation

## The Validation Tasks
The validation of REAM includes:
*  choosing data format for sharing between languages
*  On the assumption that results will not perfectly match due to numerous issues including: floating point math, redefined methods, and ?, therefore establishing a set of standards/benchmarks that define *good enough*.
* development of validation tools
   - Identification of tools
   - Identification of responsible coders.

### 1. Output Formats
Evaluate choices for output formats that is easily transferable between languages, maintains data structure, and...?

The variables in REAM will presumably all be appended to a single R list that can be saved to an RDS file (R format) for validation in R code that is separate from REAM. Based on that assumption, the output from VB needs to eventually be imported to an R list with the same object names as REAM produced. 

And considers future requirements as ERA R code grows.


##### Format Options
Save all appropriate VB variables to:
* text files in a single folder
* a data base or single xls file.
* a single yaml file
* a json file

| Option | Pro | Con |
| :--- | :------|  :---|
| text files| easy import into R            | * maybe extra coding to export?<br> * Single variable text files would import as data frame (while the equivalent in R might be a vector, so data class inconsistencies  |
| dbase     |  single file                             | a lot of overhead  data class inconsistencies against REAM vectors |
| excel     |  single file                             | MS :weary: data class inconsistencies against REAM vectors |
| yaml      | * single file <br> * vb libraries exist to ease export? | Not clear on importing data frames when combined in single file with other objects (eg vectors)  |
| json      |                               |  |

### 2. Comparison Standards
Can a perfect numerical match between programs be expected? If not, what is *good enough*?

#### Measures of acceptable reproducible results
- Relative difference as a percentage. Can this always be used or do some results need absolute comparison?
- Direction of difference


## R package development Tasks
- Establish naming conventions by adopting a style guide.
[](url)
  - [Wickham's](http://adv-r.had.co.nz/Style.html)
  - [Google's](https://google.github.io/styleguide/Rguide.html)
- Define coding tasks
- Allocate coding tasks
- 

## GUI implementation Tasks
- Define coding tasks
- Allocate coding tasks


## Documentation Tasks

- Documentation beyond just the package functions. This could be done using the a package [Vignette](http://r-pkgs.had.co.nz/vignettes.html), or the gitlab wiki for the REAM project. Either way, the documentation would hopefully include a complete representation of the ERA math, inputs, steps of use, and outputs. A draft MS Word file (and a second appendix file) of the ERA methodology is housed on the [PSC-CTC sharepoint](https://ctc.psc.org/awg/Model%20Documentation/era). These files have be converted to markdown (using [word2md.com](https://word2md.com/) and can be found [as cohort ER analysis here](https://gitlab.com/chinook-technical-committee/programs/ream/-/wikis/methods/Cohort-ER-Analysis-(CohortAnalysis2003v1.2.doc)) and [cohak description here](https://gitlab.com/chinook-technical-committee/programs/ream/-/wikis/methods/appendix-(COSHAK-DESCRIPTION-AppendexII_94CTC88-02_SM)) and could become the wiki documentation of the ERA process and REAM. However the MS word to MD conversion was incomplete and these files will need to be heavily edited/updated with any missing tables and formulas. 

- Define documentation tasks
- Allocate documentation tasks


# Membership

The team is represented by CTC members who are either adept with programming in the R language or have a good historical knowledge of data input and output within the ERA process. Members include:
* For code development:
Jon Carey, Derek Dapp, Michael Folkes, Tommy Garrison, Galen Johnson, Nick Komick, David Leonard, Martin Liermann, Mark McMillan, Oliver Miler, Antonio Velez-Espino, Charlie Waters, and Catarina Wor.
* For testing, text contributions to wiki documentation, and unforeseen tasks: Elinor McGrath, Ethan Clemons


# Reproduction of Antonio's Data and Program Flow Chart

[ctcpedia flowchart](https://gitlab.com/-/ide/project/chinook-technical-committee/documentation-and-planning/ctcpedia/blob/master/-/flowchart_programs.md)

```mermaid
graph TD;
  
  ai["Preseason & Postseason AIs"]
  auxfiles["Auxiliary .csv files"]

  badrecords[Bad records]
  baseperiodcoshak["Base period (coshak)" ]
  bsefile[".bse file"]

  CAS[CAS Program]
  catchlimits["Preseason & postseason catch limits"]
  catchsample[Catch Sample data]
  cccfile[".ccc file"]
  ccffiles[".ccf files (2)"]
  cdsfiles[".cds files"]
  ceifile[".cei file"]
  cfiles["Cfiles (recoveries)"]
  checkccc[Check CCC]
  cm1files["command files (4)"]
  cmbxlsfile["cmb .xls file"]
  cmzfile["catch distribution cmz.csv"]
  cnmodel[Chinook<br>model]
  cnrfile[".cnr file"]
  cwtcodes[CWT Codes for valid releases]
  cwtrecovery[CWT Recovery data]

  enhfile[".enh file"]
  era[Coshak]
  explorationtools[Exploration tools of model output]
  extfpfiles[External FP files]

  fcsfile[".fcs file"]
  fpaprnfiles[".fpa & .prn files"]
  fpfiles[FP files]

  hrjdbase[hrj data base]
  hrjfiles[".hrj files (2)"]
  hrjtofpa[HRJ to FPA]

  idlfile[".idl file"]

  mataeq.datmsc["mataeq.dat<br>mataeq.msc"]
  mdlfile[".mdl file"]
  mdtables["Mortality distribution tables<br>BYER Stats<br>Survival Stats"]

  op7files["a.op7<br>b.op7<br>p.op7"]
  other_EVO["Other files (eg .evo)"]
  outBYfiles[".out files (BY)"]
  outfile[".out file (CN model)"]
  outfiles[".out files (not BY, 2)"]

  pcoh-padb["pcoh.csv<br>padb.csv"]
  pnvfile[".pnv file"]
  prnfiles[".prn files"]
  pslfile[psl file]
  pst_ch3_tables["PST Table 1 & table 2"]

  rmis[RMIS and agency data bases]  

  st2file[".st2 file"]
  stkcds[".stkcds file"]
  stkfile[".stk file"]
  
  updatecas[Update CAS]

%%__________________________________________________________________
  %% Here is where the flowchart begins

  rmis-->cwtcodes
  rmis-->cwtrecovery;
  rmis-->catchsample;
  cwtrecovery-->  updatecas;
  updatecas-->badrecords;
  catchsample-->  CAS;
  auxfiles-->  CAS;
  CAS-->cfiles;
  updatecas-->CAS;
  CAS-->updatecas;

  cwtcodes-->stkcds;
  stkcds-->updatecas;
  cwtcodes-->cdsfiles;
  %%style CAS fill:#00758f

  cdsfiles-->era;
  cfiles-->era;
  cm1files-->era;
  ccffiles-->era;
  cmbxlsfile-->era;
  pslfile-->era;

  era-->outBYfiles;
  era-->hrjfiles;
  era-->outfiles;
  era-->prnfiles;

  outBYfiles-->mataeq;
  hrjfiles-->mdtables;
  mdtables-->cmzfile;
  hrjfiles-->hrjdbase;

  hrjdbase-->spfi;
  hrjdbase-->hrjtofpa;

  spfi-->fpfiles;

  mdlfile-->fpfiles;
  baseperiodcoshak-->mdlfile;
  baseperiodcoshak-->stkfile;

  stkfile-->fpfiles;
  
  extfpfiles-->fpaprnfiles;
  hrjtofpa-->fpaprnfiles;
  fpfiles-->fpaprnfiles;

  bsefile-->cnmodel;
  idlfile-->cnmodel;
  enhfile-->cnmodel;
  cnrfile-->cnmodel;
  fpaprnfiles-->cnmodel;
  ceifile-->cnmodel;
  stkfile-->cnmodel;
  pnvfile-->cnmodel;

  mataeq-->mataeq.datmsc;
  mataeq.datmsc-->cnmodel;
  st2file-->cnmodel;
  fcsfile-->cnmodel;
  op7files-->cnmodel;

  cnmodel-->cccfile;
  cnmodel-->outfile;
  cnmodel-->pcoh-padb;
  cnmodel-->other_EVO;

  cccfile-->checkccc;
  outfile-->chkclb;
  pcoh-padb-->ai;

  checkccc-->explorationtools;
  chkclb-->explorationtools;

  ai-->pst_ch3_tables;
  pst_ch3_tables-->catchlimits;

```


### Flow chart template

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```



