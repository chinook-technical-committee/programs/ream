Development questions (we need to make a few decisions) and TODO

1. high dimension objects: Some of Coshak's objects have 4 or 5 dimensions (e.g. escapement, harvest). These are currently being represented by lists. Should we move these to highly dimensional arrays?

2.  Change the names on input lists M and D, to command_data  and stock_data respectively. 

3. Should command_file and stock_data be inputted and outputted in every function?  
   * (MF) I wrote a simple evaluation tool that evaluates returning lists vs global updates from inside a function. [here](https://gitlab.com/chinook-technical-committee/programs/ream/blob/master/devs/evaluating_data_handlingtime.R). Attempting 100,000 loops and it seems there is a negligible cost to returning the full list (37 seconds), vs returning just the updated data frame (34.5 secs), vs doing a global update of the list inside the function (35.5 secs).  So having all functions returning the complete data list might be ok. But then we are committing ourselves to that data structure...

```
> time.return
   user  system elapsed 
  22.35   14.70   37.14 

> time.return2
   user  system elapsed 
  20.37   14.10   34.52 

> time.global
   user  system elapsed 
  20.76   14.61   35.52 
```
