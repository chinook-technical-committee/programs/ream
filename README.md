# REAM

REAM is an R program which is used to run the annual CTC Exploitation Rate Analysis (ERA) process. It replaces the functionality of the previous CTC CAS and CoShak programs which were written in Visual Basic. The REAM program also relies on tag recovery data and associated non-retention parameters and fishery and stock metadata stored within the CAMP database.

Documentation for the REAM package can be accessed here:

[REAM Package Website](https://chinook-technical-committee.gitlab.io/programs/r-packages/ream)

<!--
#Wiki page no longer exists
[Wiki page](https://gitlab.com/chinook-technical-committee/programs/ream/wikis/home)

[REAM Package Help](https://chinook-technical-committee.gitlab.io/programs/ream/)
-->

The R code replaces old CAS functionality using SQL queries to CAMP database that pull tag recovery data, non-retention parameters and associated metadata (stock, fishery, age). The process replaces the historical practice of producing flat files and instead directly passes queried results from CAMP into the sections of the code replacing the CoShak ERA sub routines. .cfiles, .ccf, .cmb, .psl and .cds text files are no longer produced as part of this new process.

The code has been ported from the original CoShak Visual Basic (VB) code into R. Comments from the original version of the model were preserved. R Code was also written to maximize similarity between this version and the original VB version and functions are equivalent to the SUBs in the original code. The REAM code no longer generates .svr, .prn, .im, or .out files. The .hrj files are still produced in the same format as CoShak. All other outputs are stored in an R list object with an `.rds` suffix. 

## Installation

You can install the latest version of `ream` from [GitLab](https://gitlab.com/chinook-technical-committee/programs/r-packages/ream) with:

```r
install.packages("remotes") 
remotes::install_git("https://gitlab.com/chinook-technical-committee/programs/r-packages/ream.git") 
```

If you have a previous version of REAM installed a new version may not install if the package is in use. To install the new version try removing REAM as a package, restarting R, and then reinstalling from GitLab: 

* 1. 
```r
remove.packages("ream")
```
* 2. Restart R (shift + ctrl + F10 on Windows)
* 3. 
```r
remotes::install_git('https://gitlab.com/chinook-technical-committee/programs/r-packages/ream')
library(ream)
```
Once the package is installed, if using RStudio, you will find some basic information associated with each function using R ```help```.  You can ensure the package is installed by running the dummy test stock input data using the following example code.

```r
library(ream)
command_data <- get_example_stock_setting()
example_input <- get_example_stock_input()
ream_result <- PrimarySub(command_data, example_input)
```

Users will need to create two user-specific configuration .yaml files containing (1) path settings to connect to a locally stored version of the CAMP database (`user_settings_2025_local.yaml`) and (2) server settings to connect to the cloud-based version of the database (`user_settings_2025_server.yaml`). The `yaml` files are stored outside of the REAM repository and will need to be created and populated by each user prior to running REAM. This file only needs to be updated if path directories are changed. The file can be created and edited in Notepad++. The basic structure looks like:

```yaml
name: 'You are connecting to a local version of CAMP'
pathin: 'C:/Users/yourname/REAM/YourInputPath'
pathout: 'C:/Users/yourname/REAM/YourOutputPath'
DBfilepath: 'C/Users/yourname/CAMPDBPath'
CAMPConfig: '[CAMP Connection Details or MS Access database file name]'
```

Users will also need access to the CAMP database to analyse updated data.  A local MS Access copy of the CAMP can be directly used by the REAM through default ODBC drivers.  However, user will also need to have ODBC Driver 17 drivers installed on their computer to connect to REAM to the CAMP cloud database.

Connections to either local or cloud-based versions of the database need to be specified in the user settings YAML file with the `CAMPConfig` parameter. The CAMP database is not public and a userID and password will be required and is provided with the configuration details. Please contact either the PSC Database Manager (<mcmillan@psc.org>) to determine if you have permission to access the database and, if so, to receive necessary credentials.  

If you have connectivity issues or require additional information on how to connect to the database, please contact the PSC Database Manager at <mcmillan@psc.org>.

## Code navigation

A description of REAM function schematics and dependencies can be viewed in R using:

```{r}
library(ream)
library(pkgnet)
CreatePackageReport("ream")
```
REAM's main function is `PrimarySub()`. All other functions are called from inside `PrimarySub`. To make a call to REAM, users will need to first edit their desired stock list, and then run, the script contained in `BatchReam.R` or `BatchReamParallel.R` to improve computational speed.

In R:
```r
library(ream)
writeScript("BatchReam")
```

### REAM user options  

Users can customize the ERA using a variety of settings. In future, a Shiny user interface will be developed to improve usability. For now, user options can be made within `BatchReam` and then saved before making a call to `PrimarySub`

1. The user first needs to edit the vector of stock acronyms in `stklist`. 

The table below shows the full list of the 45 currently monitored ERA stocks. A more detailed table is also available in the CTC Exploitation Rate Analysis Technical Report (Table 2.1):

| ERA Stock Name                    | Stock Acronym | Region        | Youngest Recovery Age |
|:----------------------------------|:-------------:|:--------------|:---------------------:|
| Atnarko Summer                    | ATN           | Alaska        | 2                     |
| Chilkat                           | CHK           | Alaska        | 3                     |
| Northern SE AK                    | NSA           | Alaska        | 3                     |
| Southern SE AK                    | SSA           | Alaska        | 3                     |
| Unuk                              | UNU           | Alaska        | 3                     |
| Stikine                           | STI           | Transboundary | 3                     |
| Taku                              | TAK           | Transboundary | 3                     |
| Transboundary Rivers              | TST           | Transboundary | 3                     |
| Big Qualicum                      | BQR           | Canada        | 2                     |
| Chilliwack                        | CHI           | Canada        | 2                     |
| Nicola River Spring               | NIC           | Canada        | 3                     |
| Harrison                          | HAR           | Canada        | 2                     |
| Kitsumkalum Summer                | KLM           | Canada        | 3                     |
| Kitsumkalum Yearling              | KLY           | Canada        | 3                     |
| Quinsam Fall                      | QUI           | Canada        | 2                     |
| Robertson Creek                   | RBT           | Canada        | 2                     |
| Lower Shuswap River Summers       | SHU           | Canada        | 2                     |
| Cowichan                          | COW           | Canada        | 2                     |
| Middle Shuswap                    | MSH           | Canada        | 2                     |
| Puntledge                         | PPS           | Canada        | 2                     |
| Cowlitz Fall Tule                 | CWF           | Southern US   | 2                     |
| Elk River                         | ELK           | Southern US   | 2                     |
| Elwa Fall Fingerling              | ELW           | Southern US   | 2                     |
| George Adams Fall Fingerling      | GAD           | Southern US   | 2                     |
| Hanford Wild                      | HAN           | Southern US   | 2                     |
| Hoko Fall Fingerling              | HOK           | Southern US   | 2                     |
| Columbia Lower River Hatchery     | LRH           | Southern US   | 2                     |
| Lewis River Wild                  | LRW           | Southern US   | 2                     |
| Lyons Ferry Fingerling            | LYF           | Southern US   | 2                     |
| Lyons Ferry Yearling              | LYY           | Southern US   | 3                     |
| Nisqually Fall Fingerling         | NIS           | Southern US   | 2                     |
| Nooksak Fall Fingerling           | NKF           | Southern US   | 2                     |
| Nooksak Spring Fingerling         | NSF           | Southern US   | 2                     |
| Philips River Fall                | PHI           | Southern US   | 2                     |
| Queets Fall Fingerling            | QUE           | Southern US   | 2                     |
| Samish Fall Fingerling            | SAM           | Southern US   | 2                     |
| Skagit Spring Fingerling          | SKF           | Southern US   | 2                     |
| Skyhomish                         | SKY           | Southern US   | 2                     |
| Similkameen                       | SMK           | Southern US   | 3                     |
| Skagit Summer Fingerling          | SSF           | Southern US   | 2                     |
| Tsoo-Yess Fall Fingerling         | SOO           | Southern US   | 2                     |
| Spring Creek Tule                 | SPR           | Southern US   | 2                     |
| South Puget Sound Fall Fingerling | SPS           | Southern US   | 2                     |
| South Puget Sound Fall Yearling   | SPY           | Southern US   | 2                     |
| Salmon River                      | SRH           | Southern US   | 2                     |
| Stillaguamish                     | STL           | Southern US   | 2                     |
| Columbia Summers                  | SUM           | Southern US   | 2                     |
| Upriver Brights                   | URB           | Southern US   | 2                     |
| White River Spring Yearling       | WRY           | Southern US   | 2                     |
| Williamette Spring                | WSH           | Southern US   | 3                     |

2. Characteristics relating to the start age and the years of available data are automatically generated. However, users still have the option to edit some limited parameter settings in the list `command_data` in the function get_def_command_setting(). However, users should be cautious when deviating from any of the defaults listed below.

```r
  command_data <- list(
    isCombineAge2And3 = FALSE,
    isCombineAge5And6 = ifelse(stock_code %in% c('ATN', 'HOK', 'QUE', 'QUI', 'SOO', 'SRH'), TRUE, FALSE), 
    isAge2to6Stock =  ifelse(stock_code %in% c('KLM', 'KLY', 'ATN', 'HOK', 'QUE', 'QUI', 'SOO', 'SRH'), TRUE, FALSE),
    CurrentStock = stock_code,
    ArithmeticMeanFlag = TRUE,
    NumAvgYears = 6,
    StartCoShakCY = 2000, # CW: Only used if incomplete brood is used, but must be present. 
    lastCYGUI = ifelse(Region == "SUS", ERAyear - 2, ERAyear - 1), # using this in funcCWDBRecovery, Canada would be 2022 and US would be 2021
    useIncompBrood = FALSE,
    IncompleteBroodValue = 2016, # still used?
    SkipMostRecentBY = FALSE,
    Region = Region,
    console_only = console_only
  )
)
```
Most objects are initialized in the `init_stock_data()` function . If you are unsure about the dimensionality of an object, check it out there. Additional constants are added to command_data in the `StartFormconst` function.

### Key Outputs

All major outputs are stored in the R list object `stock_data`. Input settings for a particular model run are stored in the R list objects `data_list` and `command_data`. By default, two stock_data sub-lists are created. One set of outputs runs the ERA by aligning data by brood year for non-retention (shaker) calculations (`stock_data$BY_era`), while the other option aligns data by calendar year (`stock_data$CY_era`) (e.g. for calendar year exploitation rate calculations). Upon completion, the output lists are saved within a single R object called `A.RDS` in stock-specific sub-folders within the path designated by the user in their `yaml` files.

A full description of all the elements of the elements in the object `A.RDS` can be found [here] INCLUDE PATH.

In addition, the REAM code creates two text files which are also saved to the user-specified output directly. These harvest rate or ".hrj" files are produced to match the "brood year" and "calendar year" stock_data objects and contain formatted tables of coded wire tag landed catch recoveries by stock, age, and ERA fishery, and estimated recoveries representing total mortality (including shaker and drop-off mortality), cohort population size and annual escapement recoveries by stock and age. The `BY.hrj` and `CY.hrj` files are used in downstream CTC modelling processes.

Other text file outputs previously generated by CoShak, including the total harvest rate by age (THR.prn), ocean harvest rate by age (THS.prn), survivorship analysis by age (SVR.prn), incidental mortality by age, adult equivalents, and other data contained within the .out files can now all be extracted from `A.RDS`

### 2023 Test data

A folder on SharePoint will house test data the formatted text files used in the ERA through CoShak. It will also house the HRJ files that correspond to those input files.

The in files for the 2023 ERA are [here](https://psconline.sharepoint.com/:f:/r/sites/CTC_365/Analytical%20Working%20Group/3_Exploitation%20Rate%20Analysis/2023/COSHAK_IN?csf=1&web=1&e=TOC3QN)

The out files for the 2023 ERA are [here](https://psconline.sharepoint.com/:f:/r/sites/CTC_365/Analytical%20Working%20Group/3_Exploitation%20Rate%20Analysis/2023/COSHAK_OUT?csf=1&web=1&e=e5b8SQ)
