#' organize survival rate data from CAMP
#'
#' This function uses start and ends years and survival rate
#' from the ream_pnv_params table. Previously data was read in from the .PSL files.
#' The function partially replaces the function 'GETPNVDATA' in func_readcfiles.R.
#'
#' @param command_data A list containing user input preferences
#' @param stock_data A list containing stock-specific data
#' @param data_list A list containing data from the CAMP database
#' @return The stock_data list but now containing additional data from CAMP  
#' @export


# would come from PSL file
funcREAMPNVParams <-  function(command_data, stock_data, data_list){
  
  tmpREAMPNVParams <- data_list$ream_pnv_params
  
  NatMortRte <- as.vector(unlist(c(tmpREAMPNVParams[1, which(colnames(tmpREAMPNVParams) %notin% 
                                                               c("PNVFirstYear", "PNVLastYear"))])))
  
  SurvRte <- 1 - NatMortRte
  SurvRte[length(SurvRte) + 1:2] <- SurvRte[length(SurvRte)]
  
  stock_data$StartYr <- as.double(tmpREAMPNVParams$PNVFirstYear)
  stock_data$EndYr <- as.double(tmpREAMPNVParams$PNVLastYear)
  stock_data$allCY <- tmpREAMPNVParams$PNVFirstYear:tmpREAMPNVParams$PNVLastYear # calculated in GETPNVDATA in func_flatfiles.R
  stock_data$diffbycy <- stock_data$FirstBY - tmpREAMPNVParams$PNVFirstYear # calculated in GETPNVDATA in func_flatfiles.R
  stock_data$SurvRte <- SurvRte
  
  return(stock_data)
}


#' Pulls proportion non-vulnerable and Chinook non-retention data from CAMP
#'
#' This function queries the CAMP database and pulls PNV, shaker mortalities, 
#' catch coefficients, and CNR data for each fishery from various tables. 
#' Previously data was read in from the .PSL files.
#' The function partially replaces the function 'GETPNVDATA' in func_readcfiles.R.
#' 
#' @param command_data A list containing user input preferences
#' @param stock_data A list containing stock specific information
#' @param data_list A list containing data from the CAMP database
#' @param CM1_loop Whether you are using the CY method or the BY method
#' @return The stock_data list but now containing data on PNV, shaker mortalities, catch coefficients, and CNR 
#' @export

# would come from PSL file
funcREAMPNVData <- function(command_data, stock_data, data_list, CM1_loop){
  # NatMortRte, SurvRte, StartYr, EndYr, allCY, and diffbycy are in funcREAMPNVParams now
  
  Max <- stock_data$StartAge + 3
  # Ages <- stock_data$StartAge:stock_data$MaxStkAge
  
  # initialize objects
  PNV <- lapply(1:stock_data$num_era_fisheries, 
                matrix, 
                data = 0, 
                ncol = length(stock_data$StartAge: stock_data$MaxStkAge),
                nrow = length(stock_data$allCY),
                dimnames = list(stock_data$allCY, 
                                stock_data$StartAge:stock_data$MaxStkAge))  
  
  PV <- lapply(1:stock_data$num_era_fisheries, 
               matrix, 
               data = 0, 
               ncol = length(stock_data$StartAge:stock_data$MaxStkAge),
               nrow = length(stock_data$allCY),
               dimnames = list(stock_data$allCY, 
                               stock_data$StartAge:stock_data$MaxStkAge))  
  
  ShakerMorts <- lapply(1:stock_data$num_era_fisheries, 
                        matrix, 
                        data = 0, 
                        ncol = 4,
                        nrow = length(stock_data$allCY),
                        dimnames = list(stock_data$allCY,
                                        c("empty","param1", "param2", "param3")))  
  
  CatchCoeifs <- lapply(1:stock_data$num_era_fisheries,
                        function(x, stock_data){
                          array(data = 0, 
                                dim = c(length(stock_data$allCY), stock_data$MaxStkAge, 2), 
                                dimnames = list(stock_data$allCY, 
                                                seq_len(stock_data$MaxStkAge), 
                                                c("sublegal","legal")))
                        }, 
                        stock_data)
  
  CNRVar <- lapply(1:stock_data$num_era_fisheries, 
                   matrix, 
                   data = 0, 
                   ncol = 4,
                   nrow = length(stock_data$allCY),
                   dimnames = list(stock_data$allCY, 
                                   c("param1", "param2","param3", "param4")))
  
  ShakCalcMethd <- matrix(0, 
                          nrow = length(stock_data$allCY), 
                          ncol = stock_data$num_era_fisheries,
                          dimnames = list(stock_data$allCY, 
                                          1:stock_data$num_era_fisheries))
  
  ActCatch <- matrix(0, 
                     nrow = length(stock_data$allCY), 
                     ncol = stock_data$num_era_fisheries,
                     dimnames = list(stock_data$allCY, 
                                     1:stock_data$num_era_fisheries))
  
  
  YearCat <- list(NoShak = matrix(0, 
                                  nrow = length(stock_data$allCY), 
                                  ncol = stock_data$num_era_fisheries,
                                  dimnames = list(stock_data$allCY, 
                                                  1:stock_data$num_era_fisheries)),
                  WShak = matrix(0, 
                                 nrow = length(stock_data$allCY), 
                                 ncol = stock_data$num_era_fisheries,
                                 dimnames = list(stock_data$allCY, 
                                                 1:stock_data$num_era_fisheries)))
  
  #New naming convention - this could possibly be moved into func_initializeobj.R but would need to re-order function calls in primarysub as it requires stock_data$allCY
  mortality_CR_Annual <- list(kept_legal = matrix(0, 
                                  nrow = length(stock_data$allCY), 
                                  ncol = stock_data$num_era_fisheries,
                                  dimnames = list(stock_data$allCY, 
                                                  1:stock_data$num_era_fisheries)),
                  IM_sublegal = matrix(0, 
                                 nrow = length(stock_data$allCY), 
                                 ncol = stock_data$num_era_fisheries,
                                 dimnames = list(stock_data$allCY, 
                                                 1:stock_data$num_era_fisheries)))
  
  Selectivity <- matrix(0, 
                        nrow = command_data$MaxNumFish, 
                        ncol = 2,
                        dimnames = list(1:command_data$MaxNumFish, 
                                        c("NoShak", "WShak")))
  
  lastPSLyr <- stock_data$EndYr # this is the default for most situations
  
  if(stock_data$EndYr > (stock_data$LastBY + stock_data$MaxStkAge)){
    lastPSLyr <- stock_data$LastBY + stock_data$MaxStkAge
  }
  
  YRS <- seq_along(stock_data$allCY)
  Ages <- seq_along(stock_data$StartAge:Max) # see line 8
  
  # get data from database
  tmpREAMPNVData <- data_list$ream_pnv_data
  tmpCAMPFisheryERA <- data_list$camp_fishery_era
  tmpREAMCNRData <- data_list$ream_cnr_data_weighted
 
  # stock_data$CCFStock made in funcREAMParamsCalcs (stock from AvgQFile) 
  # because catch coefficients can be used from a proxy stock and not the stock_data$Stock
  if(CM1_loop == 1){
    # by brood year
    tmpREAMCatchCoefficients <- data_list$ream_catch_coefficients[[1]]
    
  } else if(CM1_loop == 2){
    # by calendar year
    tmpREAMCatchCoefficients <- data_list$ream_catch_coefficients[[2]]
  }
  

  # loop over fisheries
  for(f in 1:stock_data$num_era_fisheries){ # f = TempNumFish in old code
    tmpDatPNV <- tmpREAMPNVData[tmpREAMPNVData$fishery_era_id == f, 
                                which(colnames(tmpREAMPNVData) == "PNVAge1"):which(colnames(tmpREAMPNVData) == "DropOff")]
    
    tmpDatCNR <- tmpREAMCNRData[tmpREAMCNRData$fishery_era_id == f, ] # for ShakCalcMethd
    
   # if(f < 78){ # otherwise gets filled with NAs rather than 0s and causes TotNumNonVuln to be NA but doesn't seem to affect code
    PNV[[f]][YRS, Ages] <- unlist(tmpDatPNV[YRS, Ages])
    PV[[f]][YRS, Ages] <- unlist(1 - PNV[[f]][YRS, Ages])
    
    # if age 6 exists, then let age 6 = age 5
    if(Max < stock_data$MaxStkAge){
      PNV[[f]][YRS, length(Ages) + 1] <- unlist(PNV[[f]][YRS, length(Ages)])
      PV[[f]][YRS, length(Ages) + 1] <- unlist(PV[[f]][YRS, length(Ages)]) 
    }
    
    ShakerMorts[[f]][YRS, 2:4] <- unlist(tmpDatPNV[YRS, length(Ages) + 1:3])
   # } # end of if f < 78
    
    stock_data$CNRFlg <- as.character(as.numeric(tmpCAMPFisheryERA$pnv_cnr_flag[f]))
    
    if(stock_data$CNRFlg == "1"){ # CNRFlg
      
      Selectivity[f, 1] <- as.numeric(tmpCAMPFisheryERA$pnv_legal_sensitivity[f])
      Selectivity[f, 2] <- as.numeric(tmpCAMPFisheryERA$pnv_sublegal_sensitivity[f])
      
      for(YR in YRS){   
        ShakCalcMethd[YR, f] <- as.character(tmpDatCNR$CNRType[YR]) # tmpDatCNR$fishery_era_id == f done above
       
        switch(ShakCalcMethd[YR, f], # CNRVar will hold the parameter values for either Method 1 or Method 2
               "0" = {
                 # Go to next line
               },
               "1" = {
                 # ShakCalcMethd%(YR, i) = 1 (Season Length Method to estimated CNR mortality)
                 CNRVar[[f]][YR, 1] <- as.numeric(tmpDatCNR$CNRValue1[YR])
                 CNRVar[[f]][YR, 2] <- as.numeric(tmpDatCNR$CNRValue2[YR])  
               },
               "2" = {
                 # ShakCalcMethd%(YR, i) = 2 (Externally Supplied Encounters of Legals and Sub-Legals)
                 CNRVar[[f]][YR, 1] <- as.numeric(tmpDatCNR$CNRValue1[YR])
                 CNRVar[[f]][YR, 2] <- as.numeric(tmpDatCNR$CNRValue2[YR])  
                 ActCatch[YR, f] <- as.numeric(tmpDatCNR$CNRValue3[YR])
               },
               "3" = {
                 # when there is no directed fishery - only a CNR fishery
                 # input current year effort - boat days fished
                 # input season length - days
                 # input reavailability time - days
                 CNRVar[[f]][YR, 1] <- as.numeric(tmpDatCNR$CNRValue1[YR])
                 CNRVar[[f]][YR, 2] <- as.numeric(tmpDatCNR$CNRValue2[YR])  
                 CNRVar[[f]][YR, 3] <- as.numeric(tmpDatCNR$CNRValue3[YR])
                 
                 CatchCoeifs[[f]][YR, Ages, "legal"] <- unlist(tmpREAMCatchCoefficients[which(tmpREAMCatchCoefficients$fishery_era_id == f & 
                                                                                                tmpREAMCatchCoefficients$SizeClass == "L"), Ages + stock_data$MaxStkAge])
                 CatchCoeifs[[f]][YR, Ages, "sublegal"] <- unlist(tmpREAMCatchCoefficients[which(tmpREAMCatchCoefficients$fishery_era_id == f & 
                                                                                                   tmpREAMCatchCoefficients$SizeClass == "S"), Ages + stock_data$MaxStkAge])
               },
               
               "4" = {
                 # New method that is a hybrid of method 1 and 2 for when just legal size kept and released are available (but no sublegals)
                 #Legal Catches
                 ActCatch[YR, f] <- as.numeric(tmpDatCNR$CNRValue1[YR])
                 #Legal Releases
                 CNRVar[[f]][YR, 2] <- as.numeric(tmpDatCNR$CNRValue2[YR])  
               },
               
               "5" = {
                 # New method for MSF fisheries
                
                 #Weighted MRR
                 CNRVar[[f]][YR, 1] <- as.numeric(tmpDatCNR$CNRValue1[YR])
                 #Weighted UKR
                 CNRVar[[f]][YR, 2] <- as.numeric(tmpDatCNR$CNRValue2[YR])  
                 # Sublegal releases for fisheries that switched from method 2 to method 5
                 CNRVar[[f]][YR, 4] <- as.numeric(tmpDatCNR$CNRValue4[YR])  
                 #Still need ActCatch for fisheries that switched from method 2 to method 5
                 ActCatch[YR, f] <- as.numeric(tmpDatCNR$CNRValue3[YR])
               }
              )
      }
      if(!stock_data$CNRFlag){stock_data$CNRFlag <- TRUE}
    }
  }
  
  stock_data$PNV <- PNV
  stock_data$PV <- PV  
  stock_data$ShakerMorts <- ShakerMorts      
  stock_data$CatchCoeifs <- CatchCoeifs
  stock_data$CNRVar <- CNRVar
  stock_data$ShakCalcMethd <- ShakCalcMethd
  stock_data$ActCatch  <- ActCatch 
  stock_data$YearCat <- YearCat # initialized here but filled in CalcLegal
  #New refactoring of stock_data$YearCat
  stock_data$mortality_CR_Annual <- mortality_CR_Annual
  stock_data$Selectivity <- Selectivity
  
  return(stock_data)
}
