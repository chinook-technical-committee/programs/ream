---
title: "Weighted Mark Release Rate"
author: "Nicholas Komick"
output: bookdown::html_document2
link-citations: yes
vignette: >
  %\VignetteIndexEntry{Weighted MRR and UKR}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
bibliography: "reamdoc.bib"
---

# Weighted Mark Release Rate (MRR)

When estimating fishery related mortality within a fishery consisting of a mix of mark-selective and non-selective regulations, an aggregate mark-release rate ($\mathscr{M}$) is needed for estimating incidental CWT mortalities associated with the fishery. Mark-release rates for Chinook fisheries are provided at a "fine scale fishery" level, where fisheries are temporally or spatially defined to capture a stratum with similar stock composition and MRRs. These rates are defined by fine scale fishery and year but are not stock-specific.

$$
\mathscr{M}_{f} = \frac{R_{f}}{R_{f} + C_{f}}
$$

In the REAM model, fishery information is aggregated at the level of Exploitation Rate (ERA) Fisheries for the purpose of the cohort analysis. ERA fisheries are comprised of one or more fine scale fisheries with different MRR rates and stock compositions. For a given ERA fishery ($F$) and stock ($s$), the ERA fishery mark release rate ($\mathscr{M}_{F,s}$) is defined as the average $\mathscr{M_{f}}$ weighted by the stock-specific encounters ($E_{f,s}$) associated with each fine scale fishery.

$$
\mathscr{M}_{F,s} = {\sum_{f}} \left(\frac{E_{f,s} }{\sum_{f}{E_{f,s}}}*\mathscr{M_{f}}\right), f \in F
$$

Where the encounters of a particular stock ($s$) within a fine scale fishery ($E_{f,s}$) is defined below based on the estimates of legal-sized marked kept ($C$) and released ($R$) Chinook:

$$
E_{f,s} = C_{f,s} + R_{f,s}
$$

However, because $R_{f,s}$ are not directly observed, the above equations are re-arranged to solve for $R_{f,s}$ as a function $\mathscr{M}_{f}$ and $C_{f,s}$ using:

$$
R_{f,s} = C_{f,s}\frac{\mathscr{M}_{f}}{1 - \mathscr{M}_{f}}
$$

Stock-specific encounter rates are then be re-written as a function of $C_{f,s}$ and ${\mathscr{M}_{f}}$:

$$
E_{f,s} = C_{f,s} \left(1 + \frac{\mathscr{M}_{f}}{1 - \mathscr{M}_{f}}\right)
$$

Inserting this into the equation for ERA fishery mark release rate produces the formula used for calculating stock-specific weighted average MRRs (${\mathscr{M}_{F,s}}$) for each ERA ($F$) within REAM: 

$$
\mathscr{M}_{F,s} = {\sum_{f}} \left(\frac{ C_{f,s} \left(1 + \frac{\mathscr{M}_{f}}{1 - \mathscr{M}_{f}}\right)}{\sum_{f}{C_{f,s} \left(1 + \frac{\mathscr{M}_{f}}{1 - \mathscr{M}_{f}}\right)}}*\mathscr{M_{f}}\right), f \in F
$$

# Weighted Umarked Kept Rate (UKR)

When fisheries are not purely mark-selective, there may also be a portion of the unmarked population which is kept. This could occur within a fine scale fishery itself or as the result of combining across a combination of fine scale fisheries with varying regulations when rolling data up to the ERA fishery level. Therefore, an unmarked kept rate (UKR) is also required at the fine scale, and aggregate, fishery levels ($\mathscr{U_{F,s}}$). The weighting applied to the UKR values is assumed to be the same as for the weighted MRR as CWT recoveries ($C_{f,s}$) are only available for the marked population.

The resulting weighted average unmarked kept rate for a given ERA fishery is then represented as:

$$
\mathscr{U}_{F,s} = {\sum_{f}} \left(\frac{ C_{f,s} \left(1 + \frac{\mathscr{M}_{f}}{1 - \mathscr{M}_{f}}\right)}{\sum_{f}{C_{f,s} \left(1 + \frac{\mathscr{M}_{f}}{1 - \mathscr{M}_{f}}\right)}}*\mathscr{U_{f}}\right), f \in F 
$$


# Variables and Symbols

| Variable | Description            | 
|:---------|:-----------------------|
| $E$      | Legal-sized mark encounters |
| $C$      | Legal-sized mark kept catch |
| $R$      | Legal-sized mark releases   |
| $\mathscr{M}$ | Mark Release Rate      |
| $\mathscr{U}$ | Unmarked Kept Rate     |

| Subscript | Description               |  
|:----------|:--------------------------|
| $s$       | Indicator stock           |
| $f$       | Fine scale fishery        |
| $F$       | Exploitation Rate fishery |
