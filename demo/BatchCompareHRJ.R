

# comments ----............................................................
#Author: Noel Swain (DFO) July 4, 2023
#demo of how to import one or more sets of HRJ files for comparison using function compareHrjFilesLong() function from package hrjexport
# (eg same stock as produced in two different analysis years OR coshak vs ream)
# outputs the results to R workspace for subsequent investigation/manipulation/summary
# includes 
# - actual values from the 1st and 2nd HRJ file in each comparison
# - difference
# - absolute difference
# - % difference, e.g., "abs(1st hrj - 2nd hrj)/((1st hrj + 2nd hrj)/2)"
# - is in long format with one column for data type to simplify subsequent manipulation/summary 
# - for all relevant data values in HRJ files (e.g., "lc_aeq",	"tm_aeq",	"lc_nom",	"tm_nom",	"cs_or_ts", "escapement",	"canada_stray_esc", and	"us_stray_esc")
# alternative to:
# - library(hrjexport) ~ compareHrjFiles() which is similar, but formats comparisons differently and exports directly to a xlsx file
# - library(ctctools)/HRJfunctionsMH.R ~ compareHRJtext2, which runs comparisons on individual pairs of HRJs and combines BY and CY estimates.
# .........................................................................
# install packages:----
#remotes::install_git("https://gitlab.com/chinook-technical-committee/programs/r-packages/hrjexport") 
library(hrjexport)
library(ream)
library(here)
library(dplyr)
library(tidyr)

# specify file paths: ----
# with usersettings file:
# ** read the help file on creating a user_settings.config file:
# ?ream::writeSettingsfile

usersettings <- ream::readSettingsfile("user_settings2024.config")
dir_old <- usersettings$validationpath
dir_new <- usersettings$pathout

# or specify directly:
#dir_old <- here("C:/Users/.../coshak_hrjs")
#dir_new <- here("C:/Users/...ream_hrjs")

### specify a list of stocks to run comparison for: ----
# stklist <- c(# Southern US
#   "CWF", "ELK", "ELW", "GAD", "HAN", "HOK", "LRH", "LRW",
#   "LYF", "LYY", "NIS", "NSF", "QUE", "SAM", "SKF",
#   "SKY", "SMK", "SOO", "SPR", "SPS", "SRH", "SSF", "STL","SUM",
#   "URB", "WSH", #, "GRO", "GRN", "ISS", "PSS
#   #  # SPS is a superstock of PSS, GRO, GRN, and ISS
#   # # Alaska
#   "ACI", "ADM", "AHC", "AKB", "ALP","ANB",
#   "CHK", "NSA", "SSA", "STI", "TAK", "TST", "UNU",
#   # # TST is a superstock of STI and TAK
#   # # SSA is a superstock of AHC ADM, ALP, and ANB
#   # # NSA is a superstock of ACI
#   # # AKB is an internal superstock of AHC, ADM, and ANB
#   # Canada
#   # have 2019 BY pseudos:
#   "ATN", "BQR", "CHI", "HAR", "KLM", "KLY", "MSH", "PPS", "RBT", 
#   # don"t have 2019 BY pseudos:
#   "COW", "NIC", "PHI", "QUI", "SHU"
# )

# compare the two sets of HRJs: ----
comparehrj <- hrjexport::compareHrjFilesLong(dir_ream, dir_coshak)

# to filter by stklist:
# comparehrj<-compareHrjFilesLong(dir_ream, dir_coshak) |> filter( stock_code %in% stklist)

# Save comparison of differences to validation path: ----
write.csv(comparehrj, paste0(dir_old, '/hrjcompare.long.csv'), row.names = FALSE)
# or specify own filepath:
# write.csv(comparehrj, 'c:/users/.../hrjcompare.long.csv', row.names = FALSE)

# filter only instances where values are different:
comparehrjsub <- comparehrj |> filter(abs.diff!=0); unique(comparehrjsub$stock_code)

# summary of differences: ----

# example of how function output could be summarized....
# without cs or ts (many duplicates of data in these):

# % identical:
round(nrow(comparehrj |> filter(abs.diff == 0, data.type != 'cs_or_ts'))/nrow(comparehrj |> filter(data.type != 'cs_or_ts'))*100, 2)
# % with differences greater than 1:
round(nrow(comparehrj |> filter(abs.diff > 1, data.type != 'cs_or_ts'))/nrow(comparehrj |> filter(data.type != 'cs_or_ts'))*100, 2)
# % with differences greater than 0:
round(nrow(comparehrj |> filter(abs.diff > 0, data.type != 'cs_or_ts'))/nrow(comparehrj |> filter(data.type != 'cs_or_ts'))*100, 2)

# with cs or ts (so conflated by duplicates)
round(nrow(comparehrj |> filter(abs.diff == 0))/nrow(comparehrj)*100, 2)
round(nrow(comparehrj |> filter(abs.diff > 0))/nrow(comparehrj)*100, 2)
round(nrow(comparehrj |> filter(is.na(abs.diff)))/nrow(comparehrj)*100, 2)

# maximum differences only for cases where differences arose:
sub_summary <- comparehrj |>
  filter(abs.diff != 0) |> 
  group_by(stock_code, yrType, data.type, age) |> 
  summarise(abs.diff = max(abs.diff, na.rm = TRUE),
            perc.diff = max(perc.diff, na.rm = TRUE))

# maximum differences for all cases (so will include cases where no differences were observed for full timeseries...)
summary <- comparehrj |> 
  filter(!is.na(diff)) |> 
  group_by(stock_code, yrType, data.type) |> 
  summarise(abs.diff = max(abs.diff, na.rm = TRUE),
            perc.diff = max(perc.diff, na.rm = TRUE))

#split into wide format for easier review:
diffs_wide <- summary |> 
  pivot_longer(cols = c(abs.diff, perc.diff), names_to = 'diff.type', values_to = 'diff') |> 
  mutate(lookup = paste0(stock_code, yrType), # just a lookup for a "report"style formatted table in the below workbook...)
         data.type = paste(diff.type, data.type, sep = "_")) |> 
  select(-diff.type) |> 
  pivot_wider(names_from = 'data.type', values_from = 'diff') |> 
  select(lookup, stock_code, yrType, 
         abs.diff_lc_nom, 
         abs.diff_tm_nom, 
         abs.diff_lc_aeq, 
         abs.diff_tm_aeq, 
         abs.diff_escapement, 
         abs.diff_cs_or_ts, 
         abs.diff_canada_stray_esc,
         abs.diff_us_stray_esc,
         perc.diff_lc_nom,
         perc.diff_tm_nom,
         perc.diff_lc_aeq,
         perc.diff_tm_aeq,
         perc.diff_escapement,
         perc.diff_cs_or_ts,
         perc.diff_canada_stray_esc,
         perc.diff_us_stray_esc)

