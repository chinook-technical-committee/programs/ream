# packages ----------------------------------------------------------------
# Install packages ------
# remotes::install_git('https://gitlab.com/chinook-technical-committee/programs/r-packages/ream', ref='main')
library(ream)

# setup -------------------------------------------------------------------

user_setting_filename <- "user_settings_2025_server.yaml"
# user_setting <- readSettingsfile(user_setting_filename)

# create command_data
command_data <- get_def_command_setting(stock_code = "URB", 
                                        ERAyear = 2025, 
                                        # provide user settings file for either a local or server version of the .yaml file: 
                                        user_setting_filename = user_setting_filename, 
                                        console_only = FALSE)

# run REAM
A <- PrimarySub(command_data = command_data, 
                data_list = NULL)

# save RDS
saveRDS(A, file = paste(A$command_data$outpath, 'A.RDS', sep = '/'))

# Disconnect from CAMP
# DBI::dbDisconnect(camp_conn)

# output ------------------------------------------------------------------

# command_data <- A$command_data
# stock_data_CY <- A$allcm1stock_data[["CY_era"]]
# stock_data_BY <- A$allcm1stock_data[["BY_era"]]

