
## REAM 2025.0.0

* Removed the use of coarse/c-file fisheries and associated rounding, now going directly from finescale to ERA fisheries
* Updated input data names and SQL code to reflect renamed tables/columns in CAMP2025
* Restructured test stocks to make it easier to add multiple test stocks
* Added helper functions to make it easier to use test stocks in example/demo code
* Simplified `PrimarySub(...)`, user settings now combined into command settings

## REAM 2024.1.0

* REAM w. CAMP2024MSF
* Updated error testing, code re-factoring, corrections to CNR algorithms
* Incorporation of marked release rate (MRR) and unmarked kept rate (UKR) data as part of new MSF algorithms

## REAM 2024.0.1

* Added demo_oufiles script to generate excel file of outfile information from RDS

## REAM 2024.0.0

* REAM w. CAMP 2024
* Removal of Alaskan expansion factors from REAM code as this expansion has been moved into CETL
* This version of REAM is compatible with CAMP 2024 but should NOT be run with CAMP 2023

## REAM 1.1 

* REAM w. flat files and CNR method 3 correction for age 3-6 stocks. 
* Fixed indexing of age 3-6 stocks for pulling of catch coefficients for CNR method 3 fisheries. 
* See issue [#110](.issues/110)

## REAM 2.1

* REAM w. CAMP 2023 and CNR method 3 correction for age 3-6 stocks
* Fixed indexing of age 3-6 stocks for pulling of catch coefficients for CNR method 3 fisheries.
* See issue [#110](.issues/110)

## REAM 2.0

* REAM w. CAMP 2023 
* Initial relase of REAM w. CAMP 
* Cannot be used with CAMP2024 or newer databases

## REAM 1.0 

* REAM w. flat files
* Initial release of REAM w. flat files (i.e. CoShak in R) 