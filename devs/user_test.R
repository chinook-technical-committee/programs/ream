#=========================================================================
#handy scripts to be ran during development 
#do not expect any organization or documentation in this file
#=========================================================================

#this sources all function files and will load over package functions:
rm(list=ls())
R.filenames <- list.files("../R", full.names = TRUE)
lapply(R.filenames, source)

# the out folder is not on git as it's holding temorary output.
# make the dir if needed
if(! dir.exists("out")) dir.create("out")

# need the cmb xls file, which is found in devs/testtxt, but .xls are ignored by git.
# Users! be sure to have a copy of STD81FISHCMB_v10.xlsx in devs/testtxt


#initial list of settings


#initial list of settings
command_data <- list(
  isCombineAge2And3=FALSE,
  isCombineAge5And6=TRUE,
  isAge2to6Stock=FALSE,
  CurrentStock ="HAR",
  ArithmeticMeanFlag=TRUE,
  NumAvgYears=9,
  LongTermAvgFlag=TRUE,
  useCFiles = TRUE,
  whichDB = "CAS",
  StartCoShakCY = 2000, # CW: Only used if incomplete brood is used, but must be present. 
  datbse="../devs/testtxt/CASClient_2019_BE.mdb",
  pathin ="../devs/testtxt/",
  pathcmb = "../devs/testtxt/STD81FISHCMB_v10.xlsx",
  useCFiles=FALSE,
  ShakerMethodType=1, #could be 1 or 4, bu 4 is not implemented 
  outpath="../devs/out/",
  useIncompBrood=FALSE,
  IncBYRadio = "Historic", #can be "Historic" or "New"
  IncompleteBroodValue=2016,
  SkipMostRecentBY=FALSE

  )

# If you get an error while running the PrimarySub() function, reach out to Catarina or Michael for a missing .xlsx file. 


# next line will end with comment in console: CHECK: developer break, things are working up to this point! yay!
A <- PrimarySub(command_data)
str(A)
names(A)
names(A$command_data)
names(A$stock_data)

A$stock_data$Escape_HRJ_NoStrays

command_data <- A$command_data
stock_data <- A$stock_data
names(command_data)


A$stock_data$InputNumFish
A$stock_data$terminal
A$stock_data$fishName
A$stock_data$CFileFishNames$Name


A$stock_data$CWTRel
A$stock_data$NumStdFisheries
fishName

"CWTRelease" %in% names(stock_data)


A$D$CWTRelease
A$BY
A$CM1_loop

A$stock_data$CWTRel


names(stock_data$harvest)
names(stock_data$harvest[[1]])