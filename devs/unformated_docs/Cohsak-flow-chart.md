```mermaid
graph TD;
  
  ccffiles[".ccf files (2)"]
  cdsfiles[".cds files"]
  cfiles["Cfiles (recoveries)"]
  cm1files["command files (4)"]
  cmbxlsfile["cmb .xls file"]
  
  getcdslist[getcdslist]

  hrjfiles[".hrj files (2)"]
 
  idlfile[".idl file"]

  outBYfiles[".out files (BY)"]
  outfiles[".out files (not BY, 2)"]
  
  primarySub[primarySub]
  pslfile[psl file]

  StartFormconst[StartFormconst]
 

%%__________________________________________________________________
  %% Here is where the flowchart begins

 
  StartFormconst-->getcdslist;


```