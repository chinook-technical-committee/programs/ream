Original technical note [on CTC SharePoint](https://ctc.psc.org/awg/Model%20Documentation/era/CohortAnalysis2003v1.2.doc?Web=1)

Chinook Technical Committee Analytical Work Group

Technical Note 2003-\_\_\_

COHORT ANALYSIS: DESCRIPTION OF METHODS AND THEORY
<!--Gill cleaned up md file July 2020, only formatting changes, no changes to actual text or wording, but removed figure links that did not link to anything. Added in Latex equations (hopefully correctly), but they won't render correctly on GitLab markdown. Original technical note [on CTC SharePoint](https://ctc.psc.org/awg/Model%20Documentation/era/CohortAnalysis2003v1.2.doc?Web=1)-->
__April 2003__

[[_TOC_]]


# Abbreviations

| AEQ | Adult Equivalence Factor |
| --- | --- |
| AWG | Analytical Work Group of the CTC |
| BWG | Bilateral Chinook Workgroup (precursor to CTC) |
| CA | Cohort Analysis |
| CNR | Chinook Non-Retention |
| CTC | Chinook Technical Committee |
| CWT | Coded-Wire-Tag |
| DIT | Double Index Tagging |
| ETD | Electronic Tag Detection |
| MSF | Mark Selective Fishery |
| PNV | The proportion of a population that is not vulnerable to a fishery |
| PSC | Pacific Salmon Commission |
| PST | Pacific Salmon Treaty |
| PSMFC | Pacific States Marine Fisheries Commission |
| PV | The proportion of a population that is vulnerable to a fishery (1-PNV) |
| RMIS | Regional Mark Information System |
|   |   |
|   |   |
|   |   |
|   |   |

# Chapter 1 - Background

A coastwide conservation program to rebuild depressed stocks of Chinook salmon was the cornerstone of the 1985 Pacific Salmon Treaty (PST) between the U.S. and Canada.  Agreement on the provisions of the program was made possible through bilateral development of methods and models that were founded in Cohort Analysis (CA) using coded-wire-tag (CWT) data.  These tools were developed by a Bilateral Chinook Workgroup based on methods and models used in the Georgia Strait model (Argue et al., 1982)[^1].

The rebuilding program consisted of catch ceilings on certain highly mixed stock ocean fisheries coupled with pass through obligations that required the &quot;bulk of savings&quot; to be passed through to spawning escapements.  Ceiling levels were negotiated using a model based on forward CA with the objective of reducing exploitation rates of depressed stocks over time to levels believed to be sustainable by Chinook stocks over the long term.

CA procedures using CWT data are particularly well suited to Pacific salmon because of their anadromy and semelparity.  These characteristics mean that all fish from a given release can eventually be observed in catch or escapement or accounted for by estimating incidental mortalities and assumptions regarding mortality rates.  Coastwide catch sampling programs for fish containing CWTs enable cohorts to <!--be--> largely be reconstructed from recovery data for spawning escapements and fisheries.  CA methods were initially applied to Chinook salmon by the Bilateral Chinook Workgroup in 1982, using a _Visicalc_ spreadsheet template based on _virtual population analysis_ methods reported by Argue et al.(1983)[^2].  The conduct of fisheries has been changing as the Parties have implemented fishery constraints under the PST and CA methods have been modified over time in response to various fishery management measures, such as changes in size limits or non-retention restrictions.  Various modifications to CA have been previously reported by the CTC (Starr et al., 1986; AWG memo, Nov. 02, 1987, TCCHINOOK(88)-02, Appendix II).

CA continues to lie at the core of the stock and fishery assessment methods employed by the CTC.  Estimates of stock-specific fishery impacts produced by CA provide information to monitor trends in stock-specific patterns of fishery exploitation and survival, impacts of individual fisheries, and parameters for simulation modeling.  Each year, the CTC performs an exploitation rate analysis based on CA of CWT release and recovery data using an assortment of computer programs that compute various statistics to facilitate monitoring of fishery impacts under PST agreements.

In 2003, the CTC is embarking on a major initiative to update these computer programs in anticipation of continued development of the CTC model, particularly, the expansion of stock-fishery time stratifications.  These changes require that CA methods be modified to accommodate stock-specific differences in growth, migration, and exploitation patterns.  The purpose of this note is to document the CA methods to be employed in making this transition.  These methods supercede all previously documented CA procedures of the CTC.  Changes from the CA methods reported in TCCHNIOOK(88)-02, Appendix II are summarized in Appendix A.

Notation is defined in Appendix B.

# Chapter 2.  General Description of Cohort Analysis

Cohort Analysis (CA) reconstructs the size of the population by back-calculation, starting with the escapement of the oldest aged fish, then successively adding impacts of terminal fisheries, preterminal fisheries, and natural mortality processes until the number of ocean age 2 fish is determined.

General sequence of computations:

- Escapement
- + PreSpawning Mortality
- + Terminal Fisheries
  - Reported Catch
  - Incidental Mortality Loss
- Estimate Terminal Run Size
- Compute Maturation Rates
- + PreTerminal Fisheries
  - Reported Catch
  - Incidental Mortality Loss
- Estimate Preterminal Cohort Size
- + Natural Mortality

Repeat until ocean age 2 beginning cohort is estimated[^3]

CA is performed on recoveries of CWT release groups coupled with imputed estimates of incidental fishing mortalities which are generated through the use of algorithms.

Once the Cohort is reconstructed, a variety of statistics are generated for use in exploitation rate analyses and the CTC model.  Primary statistics produced from CA include:

- Fishery Exploitation Rates[^4]
- Fishery Harvest Rates[^5] for Terminal fisheries:  
- Pre-fishery Cohort Sizes
- Maturation Rates
- Adult Equivalency Factors
- Ocean Age 1 Survival Rates

CA methods were originally developed for the purpose of generating profiles of fishery exploitation for individual stocks.  Total brood year exploitation rates derived from CA were compared with estimates of productivity of Chinook stocks to determine the magnitude of reductions required to maintain stocks at MSH escapement levels.

The statistics generated from CA provided a convenient and consistent means to characterize the observed pattern of fishery exploitation for individual stocks.  By reversing the sequence of calculations used to reconstruct the cohort, computer models could be built to explore the implications of &quot;what if&quot; strategies for rebuilding depressed stocks of Chinook.  That is, by starting with a beginning cohort size and then applying various fishery management measures, such as catch ceilings or harvest rate reductions over time, impacts on escapement and brood exploitation rates could be projected.

Since CA is performed for individual stocks, for simplicity in presentation stock subscripts will generally be omitted.

## Weighting of CWT Release Groups

Cohort Analysis can be performed on data for a single CWT release group or on aggregations of recovery data from CWT release groups.  Usually, releases are aggregated by production release type for a given stock (e.g., fingerling vs. yearling).  The current cohort analysis program permits the user to provide a list of the CWT codes to be aggregated in the ?????.CDS file.  The CTC AWG evaluated alternative weighting schemes in the mid 1980s[^6] and incorporated three alternatives for aggregating data from CWT release groups into the current cohort analysis program: (1) create a supergroup; (2) weight according to releases; and (3) weight equally.

**Supergroup:**

This method creates a supergroup by simply adding the recoveries of specified CWT release groups.  The effect of this aggregation method is to weight the CWT data by the group with the largest recoveries.

**Weight by Release Size**

This method creates a supergroup by weighting the recoveries of each CWT release group by the inverse of the mark rate at release.  This aggregation places the greatest weight on the CWT recovery data from the largest release.

**Equal Weight**

This method creates a supergroup by equally weighting the recoveries of each CWT release group.  This is performed by expanding the recoveries of each CWT group by the inverse of the mark rate at release of the largest release group.  This aggregation considers each CWT release group as an independent estimate of fishery exploitation.

The methods described in the following chapters apply regardless of the weighting scheme employed.

**Important Note:**  Since CA is performed on data for a single stock and brood year at a time, for convenience and simplicity of presentation, subscripts for brood year and stock are generally omitted in the formulas presented in Chapters 3 through 7.



# Chapter 3.  Actual CWT Recoveries

Actual CWT Recoveries represent recoveries that are _&quot;in hand&quot; -_ they are obtained from RMIS or directly from reporting agencies.  Actual recoveries employed in CA consist of observed CWT recoveries expanded for catch sampling rates, in accordance with agency protocols.

## Spawning Escapement

Data Required:

| Data | Source |
| --- | --- |
| Estimated CWT recoveries in spawning escapements by age | Reported recoveries are obtained from the Regional Mark Information System (RMIS), maintained by the Pacific States Fishery Management Commission.  If data are not completely reported, then these data are obtained directly from the agencies responsible for operation of hatchery programs. |
| Escapement Stray Rates | Obtained from agencies responsible for escapement estimation |
| Pre-Spawning Mortalities | Estimated by agency representatives to the CTC AWG (currently limited to Columbia River) |
| Information to prorate/estimate escapements by time period | Agency provided |

With implementation of multiple time steps, it will be necessary to partition escapement estimates to appropriate time strata.  In the few instances where escapement estimates are available by time period (e.g., dam counts on the Columbia River), partitioning can be done by information provided by agencies.

```math
\tag*{Eq. 3.1} SpEsc_{by,a,t} = SpEscCWT_{by,a,T} * PEsc_{by+a,t}
```
When no information available to prorate spawning escapement, some method to prorate escapement must be used to minimize potential problems with computing certain statistics (e.g., maturation rates, terminal fishery harvest rates) from CA and in forward cohort projections used for modeling.  Escapements are assumed to be prorated across time periods in proportion to the terminal fishery recoveries that occur during a particular time period in terminal fisheries.  The escapement is then computed as:

```math
\tag*{Eq. 3.2} SpEsc_{by,a,t} = SpEsc_{by,a,T} * \frac{\sum_{f=term} CWT_{by,a,f,t}}{\sum_{t=termfish}^T\sum_{f=term}CWT_{by,a,f,t}
```
To estimate escapements past fisheries, spawning escapements are then adjusted for stray rates and pre-spawning mortality losses:

```math
\tag*{Eq. 3.3}  Esc_{by,a,t} = \frac{SpEscCWT_{by,a,t}}/{(1-StrayRate_{by+a})*(1-IDL_{by+a,a})}
```
## Terminal Fisheries

Fish harvested by fisheries occurring near its river of origin are usually designated as terminal for a given stock, regardless of age.  For net fisheries outside the vicinity of a stock&#39;s river of origin, all fish age 4 and older are assumed to be mature; consequently, for fish that are destined to spawn in other areas, net fisheries can be considered to be preterminal for fish younger than age 3 and terminal for fish older than age 3.

Data Required:

| Data | Source |
| --- | --- |
| Estimated CWT recoveries in designated terminal fisheries | Estimated recoveries are obtained from RMIS.  If data are not completely reported, then these data are obtained directly from the agencies responsible for monitoring fisheries. |
| Designation of Terminal Fisheries by age and time step | Provided by agencies responsible for fishery regulation |

## Pre-Terminal Fisheries

Pre-Terminal fisheries are presumed to operate on the entire cohort alive at the start of a fishing period.  All fisheries are presumed to be Pre-Terminal unless specifically designated as Terminal for a given stock and age.

Data Required:

| Data | Source |
| --- | --- |
| Estimated CWT recoveries in designated Pre-Terminal fisheries | Estimated recoveries are obtained from RMIS. |



# Chapter 4.  Imputed CWT Recoveries

Imputed CWT recoveries represent mortalities that are not &quot;in hand&quot;, that is there is no physical tag in custody that represents a direct observation.  Imputed mortalities are estimated using assumptions and agency-supplied data, but are incorporated into CA in order to provide a complete picture of mortalities that affect a given cohort of fish throughout its lifetime.

## Natural Mortalities

Cohort Analysis assumes that natural mortalities occur at the start of a fishing period.  Direct estimates of natural (non-catch) mortality for Chinook salmon are not available.  The current cohort analysis procedure assumes the following annual mortality schedule:

| Ocean Age | Natural Mortality |
| --- | --- |
| 2 | 0.4 |
| 3 | 0.3 |
| 4 | 0.2 |
| 5+ | 0.1 |

These assumed natural mortality rates are fixed.  The rates were originally derived from those used in the Georgia Strait virtual population analysis (Argue et al., 1983).  The Argue paper used a natural mortality of 1.5% per month for ages 3 to 5 and 3% per month for age 2.  These values correspond to annual rates as follows:

```math
\tag*{Eq. 4.1}
\begin{alignedat}{1}
 &SurvivalRate(Ages 3-5) = (1-.015)^12 = 0.83\\
 &NaturalMortalityRate(Ages 3-5) = 1-SurvivalRate(Ages 3-5) = 0.17\\
 &SurvivalRate(Age 2) = (1-0.03)^12 = 0.69\\
 &NaturalMortalityRate(Age 2) = 1-SurvivalRate(Age 2) = 0.31
\end{alignedat}
```

In 1982, the bilateral Chinook Workgroup established to support on-going negotiations for a new U.S.-Canada salmon interception treaty decided to use the stepped values of mortality by age as presented above (Ricker 1976)[^7].  The ocean age 4 natural mortality rate was assumed to be 20% for age 4 fish while natural mortality rates for ocean age 5 and older fish were assumed to be 10%.  Natural mortality rates for fish ocean age and 2 fish were assumed to be 30% and 40%, respectively.  The ocean age 1 survival rate is computed as the beginning ocean age 2 cohort size (prior to natural mortality) divided by the release size.  While somewhat arbitrary, the actual values for natural mortality rates used for CA are inconsequential so long as the same rates are applied in both forward cohort projections and backwards cohort reconstruction.

When multiple time steps are considered, there are a variety of methods that can be employed, including: (a) apply natural mortality annually when the fish are aged; (b) apportion annual mortality according to the lengths of the time steps employed in the CTC Model; or (c) apportion annual mortality by the mean length of fish at the mid-point of the time steps employed in the CTC model.

  Aging:  Ocean aging occurs October 1 for fall-type stocks and July 1 for spring type stocks.

## Incidental Fishing Mortalities

Incidental fishing mortalities of fish from individual CWT releases are not directly observed, but are believed to result from effects of regulation and the operation of gear.

Current CA procedures do not attempt to estimate these mortalities during cohort reconstruction.  Rather, an iterative approach based on forward cohort reconstruction is used.  Shakers are estimated on either a brood year or calendar year basis for a single stock at a time.  For the brood year method, only CWT recoveries for a single brood year are considered.  For the calendar year method, age-specific CWT recoveries for all brood years contributing to fisheries in a calendar year are considered.  Appendix II to TCCHINOOK(88)-02 describes the procedure as:

1) Total all legal catches

2) Calculate initial cohort abundances from catch, escapement and natural mortality assumptions.  Do not consider incidental fishing mortalities.

**DO**

3) Calculate the total number of sublegals and divide by the legal catch to get the number of encounters of fish subject to shaker mortality

4) Calculate total shaker mortalities

5) Calculate total CNR mortalities

6) Allocate shaker and CNR mortalities to cohorts in proportion to estimated size of the non-vulnerable population

7) Calculate new cohort abundances

8) Compare new Age 2 cohort sizes with previous Age 2 cohort sizes

**LOOP UNTIL** all changes in Age 2 cohort sizes are less than 5%.

The forward cohort method used in this procedure are similar to, but significantly different from, those used in the CTC Model.  A major difference is that the CTC Model performs shaker calculations on all stocks simultaneously.  Besides introducing inconsistency between backward cohort reconstruction and forward projection modeling, this iterative approach is not necessary.  Incidental mortalities can be computed for each brood year during the cohort reconstruction process.  Methods to perform these calculations are presented in this Chapter.  Calculations of incidental fishing mortalities in the CTC Model would be substantially simplified while improving consistency with backwards CA if corresponding changes were to be made to CTC Model code.

CA methods account for three general types of incidental fishing mortalities: drop off, shaker, non-retention.

Data Required:

| Data | Source |
| --- | --- |
| Regulations | Agencies responsible for fishery regulation provide information regarding management measures (e.g., size limit restrictions, non-retention periods) |
| Fishery Impacts | Agencies responsible for fishery regulation provide information on the results of fishery regulation (e.g., estimates of landed catch, effort, or season length), along with monitoring data (e.g., estimated numbers of encounters of Chinook during non-retention periods). |
| Incidental Mortality Rates | Provided by agencies responsible for fishery regulation (may be reviewed by CTC).  TCCHINOOK (97)-1. |

The general schematic of mortalities in a retention fishery is depicted in figure 1: (1) fish encounter gear and are subject to drop off mortality before being brought to the boat; (2) a decision is made whether of not the fish can be legally retained; (3) if the fish can be legally retained, it appears in the landed catch; (4) if the fish cannot be legally retained, it is subject to release mortality.

```mermaid
graph TD;
  Retain-->N;
  Retain-->N;
  N-->Shaker Mortality;
  N-->Survive;
  Retain-->Y;
  Y-->Landed Catch;
  Encounter Gear-->Retain;
  Encounter Gear-->Drop off;
```

Figure 4.1

## Drop Off

Drop Off mortalities do not appear in the reported catch, but represent fish that are killed as a result of encountering the gear.  Examples of such mortalities include loss to predators like sharks or marine mammals, escape with lethal wounds, or fish taken for personal use.  Observational data that can be used to quantify drop off mortalities are scarce.  Drop Off mortalities are usually expressed as an assumed proportion of fish brought to the boat, inclusive of legal and sublegal-sized fish.

Data Required:

| Data | Source |
| --- | --- |
| CWT Recoveries | RMIS |
| Drop Off Mortality Rate | Provided by agency or CTC |

```math
\tag*{Eq. 4.2}     DO_{a,f,t} = CWT_{a,f,t}*dmr_{f,t}
```

## Shakers

Incidental mortalities of fish outside the legal retention size limit for a fishery are termed &quot;shaker loss.&quot;  Shaker mortalities are computed by applying an assumed mortality rate to the number of fish released.

Two method for dealing with shakers are considered here: (A) encounters estimated through algorithms; and (B) reported encounters.

**Encounters Estimated Through Algorithms**

The proportion of a given population outside the legal retention size limit is termed the PNV for &quot;Proportion Non-Vulnerable&quot;.  The PNVs are determined by a combination of stock-specific growth functions and specified size limits.

Data Required:

| Data | Source |
| --- | --- |
| Mean and variance of size | Estimated by AWG from length data for a given stock as obtained from RMIS – see Appendix C. |
| Release Mortality Rates | Provided by CTC.  Varies by size of fish, fishery and time (corresponding to gear restrictions such as barbless hooks or use of revival tanks with restrictions on soak times, etc.). |

**Stock-specific growth functions**

The PVs and PNVs are based on parameters derived from stock-specific growth functions. These parameters are comprised of the means () and variances () of Gaussian distributions.  Methods for parameter estimation are provided in Appendix C.

**Size Limits**

Shaker mortalities result from the impacts of size limits.  Thee types of size limits are considered: (1) minimum size limit only; (2) maximum size limit only; and (3) &quot;slot limits&quot; where both minimum and maximum size limits are in effect.  In addition, a minimum size for fish vulnerable to the gear may also be involved.  The general relationship of these values is depicted in figure 2:

![Fig. 4.2.  Size Distribution in Cohort](C:\Users\gill\Pictures\CohortERAnalysis2003_Fig.42.png)

Assuming that they are all from legally retained fish, CWT recoveries are the result of the exploitation rate on the fish that can be legally retained.

```math
\tag*{Eq. 4.3} CWT_{by,a,f,t} = ER_{by,a,f,t}*CH_{by,a}*PR_{by+a,f,t}
```

Solving for the exploitation rate

```math
\tag*{Eq. 4.4} ER_{by,a,f,t} = \frac{CWT_{by,a,f,t}}{CH_{by,a}*PR_{by+a,f,t}}
```

The proportion of the population subject to sub-legal mortalities lies below any minimum size limit and above the minimum size of fish vulnerable to the gear.  The proportion of the population subject to extra-legal mortalities lies above any maximum size limit.  For the three size limit scenarios considered:

| Proportion of Population Subject to | Minimum size limit | Maximum Size Limit | Slot Limit |
| --- | --- | --- | --- |
| Retention (PR) | 1-ND(minSL) | ND(maxSL) | ND(maxSL)-ND(minSL) |
| Sub-Legal Shakers (PNVS) | ND(minSL)-ND(minV) | None = 0 | ND(minSL)-ND(minV) |
| Extra-Legal Shakers (PNVE) | None = 0 | 1-ND(maxSL) | 1-ND(maxSL) |

Where ND(len) is a function that returns the cumulative probability of a Normally distributed random variable, that is, the probability below length (len).  Note that ND(len) can be approximated within 1% by:

```math
\tag*{Eq. 4.5} ND(len)\sim(1-exp^(-1.70*len)⁡)^(-1)
```

where _len_ is length standardized to a N(0,1) distribution using the mean and variance for the stock, age and time period (N. Balakrishan.  Handbook of the Logistic Distribution, p367).

Assume that the exploitation rate reflected by the CWT recoveries on the proportion of the population that can be retained reflects the encounter rate of fish that are subject to shaker mortality.  Using the values depicted in the previous table for each size limit scenario, the number of encounters corresponding to legally retained catch and subject to shaker mortalities can now be estimated as:

```math
\tag*{Eq. 4.6} enc?_{by,a,f,t} = ERb_{by,a,f,t}*CH_{by,a}*PNV?_{by+a,f,t} = \frac{CWT_{by,a,f,t}*PNV?_{by+a,f,t}}{PR_{by+a,f,t} }
```
And the shaker mortalities estimated as:
```math
 \tag*{Eq. 4.7}    Shak?_{by,a,f,t} = {rm?_{by+a,t}+dmr}*enc?_{by,a,f,t}
```

Where the ? refers to sub-legal (S) or extra-legal (E) sized fish. Note that the cohort size is not needed (in fact, that is what we are trying to reconstruct), so shaker mortalities can be estimated using only the CWT recoveries, information about the length distribution of the cohort, size limit restrictions, and assumed incidental mortality rates.

**Reported Encounters**

The 1999 PST Agreement contains a number of provisions directed at moving to total mortality based regimes.  It is anticipated that some agencies will institute fishery monitoring programs to obtain estimates of shakers encountered during periods of retention.

Data Required:

| Data | Source |
| --- | --- |
| Retained catch | Provided by agency |
| Shaker encounters | Provided by agency |

When such encounters are reported they can be allocated to cohorts by size and age in proportion to the CWT recoveries.  The first step is to use the methods described previously to compute the proportion of shakers that would be expected to be comprised of legal and extra-legal sized fish.  The last term in the equation reflects the proportion of the shaker population comprised of sub-legal or extra-legal sized fish.

```math
\tag*{Eq. 4.8} enc?_{by,a,f,t} = CWT_{by,a,f,t}*\frac{Re⁡portedEncounters_{by+a,f,t}}{Catch_{by+a,f,t}}*\frac{PNV?_{by+a,f,t}}{{1-PR_{by+a,f,t}}}
```
The shakers can then be estimated as:
```math
\tag*{Eq. 4.9} Shak?_{by,a,f,t} = {rm?_{by+a,t}+dmr}*enc?_{by,a,f,t}
```

Where the ? refers to sub-legal (S) or extra-legal (E) sized fish.

## Non-Retention

The general schematic for mortalities in a Chinook non-retention (CNR) fishery is depicted in figure 3.  Here, there is no landed catch, all Chinook must be released and they either die or survive.

```mermaid
graph TD;
  Encounter Gear-->Shaker Mortality;
  Encounter Gear-->Survive;
  Encounter Gear-->Drop off;
```
Figure 4.3.  General Schematic for CNR Fishery

**Selectivity Factor**
With the institution of constraints on landed catch under PST regimes, managers have frequently resorted to non-retention restrictions to access harvestable fish from other species.  Selectivity factors are employed to represent the relative change in Chinook encounter rates resulting from gear restrictions or fishing methods when fisheries are directed at other species.  Rarely are selectivity factors supported with observational data, although estimates could be derived from historical catch and effort data.  Two selectivity factors are considered: (1) one for fish that would have been retained had CNR restrictions not been imposed; and (2) the other for fish that would have been subject to shaker mortality had retention restrictions not been imposed.

Data Required:

| Data | Source |
| --- | --- |
| Selectivity Factor for fish that could be legally retained during the retention period | Agency provided. |
| Selectivity Factor for fish that would be subject to shaker mortality during the retention period | Agency provided. |

**With Landed Catch**

Chinook non-retention restrictions have been most commonly employed during only a portion of the season, once allowable limits on landed catch have been reached.  Four general methods are currently incorporated into cohort analysis procedures, depending on the availability of observational data.

**RT Method**

Under this CNR method, no observational data are employed.  Instead, non-retention mortalities are estimated through a CTC model-generated RT factor (RT is an abbreviation for &quot;ratio&quot;, since it represents the ratio between the landed catch and the catch that would have resulted under base period exploitation/harvest rates.

Data Required:

| Data | Source |
| --- | --- |
| RT factor | CTC Model from simulation of ceiling management. |
| BPER | Input data for CTC Model. |
| FP | Scalar input value for CTC Model |

The RT factor generated by the CTC model is computed as follows:

```math
\tag*{Eq. 4.10} RT_{f,t} = \frac{Catch_{f,t}}{\sum_{s}\sum_{a} CH_{s,s,t}*PR_{s,a,f,t}*FP_{s,a,f,t}*BPER_{s,a,f,t}}
```
The value (1-RT) ~ portion of fishery operating under CNR restriction.  The exploitation/harvest rate associated with the CNR fishery is (1-RT)\*BPER\*FP.  Since all fish are supposed to be released in a CNR fishery, the fish are subject to multiple encounters.

Convert the BPERs to instantaneous mortality rates under Baranov type equations for competing exponential risk (consider only fisheries that operate within a region or all fisheries?).  Compute encounters and incidental mortalities (drop off plus release) for CNR period using the following method:

For example, if we are considering a troll and sport fishery that operates simultaneously on the population

```math
\tag*{Eq. 4.11}     ZT = QS + QT
```

Then,
```math
\tag*{Eq. 4.12}     ER_{T} + ER_{S} = 1 - exp^-ZT
```

So that
```math
\tag*{Eq. 4.13}     ZT = -ln(1-ER_{T}-ER_{S}
```

Then we can solve for fishing mortality rates (or encounter rates) by
```math
\tag*{Eq. 4.14}     QS = ZT*\frac{ER_{S}}{ER_{S}+ER_{T}}
```
and
```math
\tag*{Eq. 4.15}     QT = ZT*\frac{ER_{T}}{ER_{S}+ER_{T}}
```

These instantaneous catchability coefficients then would reflect the total during the base period.  To compute mortalities during the CNR period, these rates would need to be prorated to account for release and drop off mortalities of legal-sized fish resulting from encounters.  For instance if the troll fishery operates under CNR restrictions while the sport fishery operates normally:
```math
\tag*{Eq. 4.16}     QTT = QT*(1-RT)*FP_ *SF*(rm_{T}+dmr_{T})
    ZTT = QTT+QS
```

The total mortality during the CNR fishery would then be:
```math
\tag*{Eq. 4.17}     TotalMort = CH*(1-exp^-ZTT⁡)
```

Mortality or landed catches in individual fisheries are then just obtained by partitioning the total mortality among the sources (Robert Kope, pers. Comm.).  For example, Drop-Off mortality for the Troll fishery would be

```math
\tag*{Eq. 4.18} DO = dmr_{T}*\frac{QTT}{ZTT}*CH*(1-exp^-ZTT)
```

While the release mortality would be:

```math
\tag*{Eq. 4.19}     RM = rm_{T}*\frac{QTT}{ZTT}*CH*(1-exp^-ZTT⁡)
```

The above equations would be applied for fish that could be retained.  For shaker mortalities, a corresponding set of equations, replacing base period exploitation rates for landed catch by base period shaker exploitation rates.

*ALTERNATIVE – Discrete formulation*  The instantaneous approach to estimate incidental mortalities considering multiple encounters is relatively complex compared to a discrete formulation.  Unless exploitation/harvest rates are relatively high, differences between discrete and instantaneous formulations are likely to be inconsequential.  The discrete formulation of CNR mortalities follows.  CWTs recovered during the retention period result from the following:

```math
\tag*{Eq. 4.20}     CWT_{s,a,f,t} = RT_{f,t}*CH_{s,a,t}*PR_{s,a,f,t}*FP_{s,a,f,t}*BPER_{s,a,f,t}
```

The total number of encounters of fish that could be retained had the fishery been allowed to operate under base period exploitation/harvest rates is simply:

```math
\tag*{Eq. 4.21}     \frac{CWT_{s,a,f,t}}{RT_{f,t}}
```

Therefore, the number of encounters of fish that could otherwise have been retained during the CNR period is:

```
    enc = \frac{CWT_{s,a,f,t}*(1-RT_{f,t})}{RT_{f,t}}                  Eq. 4.22
```

These encounters are adjusted to account for differences in the conduct of the fishery under CNR restrictions.  Finally, the incidental mortalities are computed to reflect drop off and release mortality rates.  Incidental fishing mortalities of fish that could otherwise have been retained is:

```
    CNRL_{s,a,f,t} = SF_{f,t,L}*{rmL_{f,t}+dmr_{f,t}}*enc              Eq. 4.23
```

Shaker mortalities during the retention period are computed using the procedures previously described in section \_\_\_.  The ratio between CWT and shaker mortalities can be used to estimate the number of shakers expected during the CNR period, adjusting for differential effects of CNR restrictions on fish subject to shaker mortality (e.g., gear restrictions may increase impacts on small fish relative to large fish).

```
    CNRS_{s,a,f,t} = \frac{SF_{f,t,S}*Shak_{s,a,f,t}*enc}}{SF_{f,t,L}*CWT_{s,a,f,t}}    Eq. 4.24
```

**Season Length**

Under this CNR method, observational data consist of the number of days the fishery is open to Chinook retention and the number of days that non-retention restrictions were in effect.

Data Required:

| Data | Source |
| --- | --- |
| Days of Retention | Agency provided. |
| Days of CNR | Agency provided. |

In this method, CNR mortalities are estimated by expanding CWT recoveries by the length of the CNR season (CNRSeas) relative to the retention season (RetSeas).  For fish that would be retained if not for CNR restrictions:

```
    CNRL_{a,f,t} = CWT_{a,f,t}*SF_{f,t,L}*(rmL+dmr)*\frac{CNRSeas_{by+a,f,t}}{Re⁡tSeas_{by+a,f,t}}                                                 Eq. 4.25
```

Similarly, mortalities of fish that would not be subject to retention during the retention period would be expanded.

```
    CNRS_{a,f,t} = Shak_{a,f,t}*SF_{f,t,S}*{rmS+dmr}*\frac{CNRSeas}{Re⁡tSeas} Eq. 4.26
```

**Effort**

Under this CNR method, observational data consist of the effort during the period when the fishery was open to Chinook retention (RetEffort) and the effort during the non-retention period (CNREffort).

Data Required:

| Data | Source |
| --- | --- |
| Effort during Retention | Agency provided. |
| Effort during CNR period | Agency provided. |

```
    CNRL_{a,f,t} = CWT_{a,t}*SF_{f,t,L}*{rmL+dmr}*\frac{CNREffort_{by+a,f,t}}{Re⁡t Effort_{by+a,f,t}}                                                 Eq. 4.27
```

```
    CNRS_{a,f,t} = Shak_{a,t}*SF_{f,t,S}*{rmS+dmr}*\frac{CNREffort_{by+a,f,t}}{Re⁡t Effort_{by+a,f,t}}                                                 Eq. 4.28
```

**Reported Encounters**

Under this CNR method, observational data consist of the number of Chinook landed, the number of legal size Chinook encountered during the non-retention period (CNRLEnc), and the number of sublegal size Chinook encountered (CNRSLeg) during the non-retention period.  Encounter data are not stock-specific.

Data Required:

| Data | Source |
| --- | --- |
| Reported Catch | Agency provided. |
| Total Encounters of &quot;Legal&quot; sized fish during CNR | Agency provided |
| Total Encounters of &quot;Sub-Legal&quot;sized fish during CNR period | Agency provided |

Assuming that reported encounter data do not distinguish fish above any maximum size limit from fish that could be legally retained under size limit restrictions, mortalities of legal and larger sized fish are simply expanded by the ratio of encounters to reported catch.

```
    CNRL_{a,f,t} = CWT_{a,f,t}*{rmL+dmr}*\frac{CNRLEnc_{by+a,f,t}}{Catch_{by+a,f,t}}  Eq. 4.29
```

With agency-provided encounter rate data, the total number of sublegals encounters is from all stocks and age classes.  In this situation, it would be desirable to allocate these encounters in proportion to the relative size of the sublegal cohorts by stock and age.  However, data on stock-age composition of encounters are not available.

The following method is proposed as a means of allocating the mortalities for cohort analysis for a single brood.  The method uses the ratio between the sublegal encounters in the CNR fishery and the reported catch to estimate the encounters of suublegal sized fish.

```
    CNRS_{a,f,t} = CWT_{a,f,t}*{rmS+dmr}*\frac{CNRSEnc_{by+a,f,t}}{Catch_{by+a,f,t}}  Eq. 4.30
```

**Slot limits**
When slot limits are in place during the retention portion of the fishery, the encounters reported by the agency may separate our encounters of legal, sub-legal, and extra-legal sized fish.  In this situation, mortalities of legal and extra-legal sized fish can can be estimated by:

```
    CNRL_{a,f,t} = CWT_{a,f,t}*{rmL+dmr}*\frac{CNRLEnc_{by+a,f,t}+CNREEnc_{by+a,f,t}}{Catch_{by+a,f,t}}                                           Eq. 4.31
```

And sub-legal mortalities by equation \_\_\_ above.

If the agency reports only total legal and shaker encounters, mortalities of extra-legal shakers can be estimated as:

```
    CNRE_{a,f,t} = CWT_{a,f,t}*(rmL+dmr)*\frac{CNRShakEnc_{by+a,f,t}}{Catch_{by+a,f,t} }*\frac{PNVE_{by+a,f,t}}{(1-PR_{by+a,f,t})}                        Eq. 4.32
```

And sub-legals as:

```
    CNRS_{a,f,t} = CWT_{a,f,t}*(rmS+dmr)*\frac{CNRShakEnc_{by+a,f,t}}{Catch_{by+a,f,t}}*\frac{PNVS_{by+a,f,t}}{(1-PR_{by+a,f,t})}                Eq. 4.33
```

**Incidental Mortalities With No Landed Catch or CWT Recoveries**

*Total Non-Retention:*  In recent years, Chinook retention has been prohibited entirely in some fisheries.  However, incidental mortalities of Chinook occur during fisheries directed at other species.  Some of the fish released during CNR fisheries are expected to survive and be vulnerable to recapture.  The potential for multiple encounters of the same fish is evaluated by converting the discrete linear catch equations normally used for cohort analysis for retention fisheries to continuous form.  The methods for estimating incidental mortalities during periods of total non-retention have already been described previously.


## Mark-Selective Fisheries

**Mark Selective Fisheries**
Mortalities associated with mark-selective fisheries (MSFs) have been conducted in certain terminal areas and may soon be implemented in pre-terminal areas.  These fisheries differ from non-retention fisheries in that fish with adipose fin clips are subjected to different mortalities than fish without adipose fin clips.  CWT recoveries of fish with clipped adipose fins in MSF can be analyzed in the same manner as with non-selective fisheries, except for the consideration of mark-recognition error and unmarked retention error.  In a normal mark-selective fishery, only fish with healed adipose fin clips may be retained.  However, some marked fish may be released (mark recognition error) and some unmarked fish may be retained (unmarked retention error).   MSF are depicted schematically in figure 4.  For marked fish, two decisions must be made: (1) is the fish marked?; and (2) Can the fish be legally retained under restrictions such as size limits?  For unmarked fish, the decision is only is the fish marked or not.

```mermaid
graph TD;
  Marked Fish-->Encounter Gear;

  Encounter Gear-->Retain;
  Encounter Gear-->Drop off;
  
  Retain-->Y;
  Retain-->N;
  
  Y-->Landed Catch;
  
  N-->Shaker Mortality- Mark Recognition Error or Outside Retention Limits;
  N-->Survive; 
```

```mermaid
graph TD;
  Unmarked Fish-->Encounter Gear;
  
  Encounter Gear-->Marked;
  Encounter Gear-->Drop off;
  
  Marked-->Y;
  Marked-->N;

  Y-->Landed Catch Unmarked Retention;
  
  N-->Shaker Release Mortality;
  N-->Survive; 
```
Figure 4.4. General Schematic for MSF Fishery

Since an important use of CWT data is to make inferences regarding fishery impacts on unmarked fish (predominantly naturally-produced fish) some means is necessary to use CWT recoveries to generate estimates of mortalities on unmarked fish.

There are two basic approaches that can be employed to generate such estimates: (1) DIT; and (2) Assumption-Based Methods.

**DIT**

The PSC Selective Fishery Evaluation Committee (SFEC) has attempted to develop methods to try to provide stock-age-fishery estimates of exploitation rates on unmarked fish using Double Index Tagging (DIT) and Electronic Tag Detection (ETD).[^8]

In DIT, CWT groups are released in pairs that are supposed to be identical, except that adipose fins are removed from one group and left intact in the other.  ETD is required as a means to detect CWTs in fish with missing adipose fins and fish from DIT groups with adipose fins intact.  Differences in CWT recovery patterns can theoretically provide a means of estimating impacts of MSFs on the unmarked DIT group.  So far, only the &quot;paired ratio&quot; method appears to mold promise as a means of estimating stock-age-fishery specific impacts.  The capacity of the DIT/ETD approach to generate reliable stock-age-fishery specific impacts has not been verified.

Agency provided MSF mortalities.  If agencies provide stock-age-fishery specific pseudo recoveries for MSFs, then the CA can proceed normally.  If such estimates are not available, assumption based methods will need to be employed.

**Assumption-Based Methods**

In many instances, impacts of MSFs are likely too small to enable reliable quantification via DIT.  Under such circumstances, assumption-based methods will likely be required.  These methods rely primarily upon four principal parameters: (1) drop off; (2) release mortality rates; (3) mark-retention error; and (4) unmarked recognition error.   Drop off has already been discussed.

**Release Mortalities**

Immediate and delayed mortality rates of fish released after capture are required by fishery strata; different rates may be applied for legal and sublegal-sized fish.

**Mark-Retention Error**

Mark-retention error rates are expressed as the probability of a fish with a missing adipose fin being released.  Released fish are of course subject to release mortality rates.

**Unmarked Recognition Error**

Unmarked recognition error rates are expressed as the probability of a fish with an adipose fin being retained.

**Multiple Encounters**

An additional consideration is the potential for released fish to survive and be subject to recapture.  Some of the fish released during non-retention fisheries are expected to survive and be vulnerable to recapture.  The potential for multiple encounters of the same fish is evaluated by converting the discrete linear catch equations normally used for cohort analysis for retention fisheries to continuous form.  Currently, cohort analysis procedures estimate encounters by computing catchability coefficients from historical estimates of exploitation rates of landed catch.  These catchability coefficients are then used to estimate the number of encounters in the MSF fishery.  Fish encountered in non-retention fisheries are subject to drop off and release mortality.

**Proposed Approach**

Convert the BPERs to instantaneous mortality rates under Baranov type equations for competing exponential risk (consider only fisheries that operate within a region or all fisheries?).

Data Required:

| Data | Source |
| --- | --- |
| Base Period Exploitation Rates for Reported Catch | CTC Model Calibration |
| Base Period Exploitation Rates for shaker mortalities | CTC Model Calibration |
| Adjustments to Base Period Conditions reflected by MSF restrictions (e.g., effort change relative to base period) | Agency provided |
| Unmarked Recognition Error &amp; Mark Retention Error | Agency provided |


Compute encounters and incidental mortalities (drop off plus release) for CNR period using the following method:

For example, if we are considering a troll and sport fishery that operates simultaneously on the population

```
    ZT = QS+QT                                                         Eq. 4.34
```

Then,

```
    ER_{T}+ER_{S} = 1-exp^-ZT                                          Eq. 4.35
```

So that

```
    ZT = -ln(1-ER_{T}-ER_{S})                                          Eq. 4.36
```

Then we can solve for fishing mortality rates (or encounter rates) by

```
    QS = ZT*\frac{ER_{S}}{ER_{S}+ER_{T}}                               Eq. 4.37
```

and 

```
    QT = ZT*\frac{ER_{T}}{ER_{S}+ER_{T}}                               Eq. 4.38
```

These instantaneous catchability coefficients then would reflect the total during the base period.  To compute mortalities under MSFs, these rates would need to be prorated to account for release and drop off mortalities of legal-sized fish resulting from encounters and for changes in fishing effort (reflected through FP factors).  Under MSFs, two additional types of mortality would need to be considered: (a) unmarked recognition error, _ure_, which accounts for the probability that an unmarked fish will be illegally retained; and (b) mark recognition error, _mre_, which accounts for the probability that a marked fish will be released during the MSF.  For instance if the sport fishery operates under MSF while the Troll fishery is non-selective:

```
    
    QTU = QTM = QT*FP*SF*(1+dmr_{T})
    QSU = QS*((1-ure)*rmS+dmr_{S}+ure)
    QSM = QS*(1-mre+dmr_{S}+mre*rmS)
    ZU = QSU+QTU
    ZM = QSM+QTM                                                       Eq. 4.39
```

The total mortality during the MSF fishery would then be:

```
    TotalMortU = CH_{U}*(1-exp^-ZU)
    TotalMortM = CH_{M}*(1-exp^-ZM)                                    Eq. 4.40
```
Mortality or landed catches in individual fisheries are then just obtained by partitioning the total mortality among the sources.  For example, mortality due to illegal sport retention would be

```
    IllegalSportCatch = \frac{QS*ure}{ZU}*CH_{U}*(1-exp^-ZU)           Eq. 4.41
```

While the legal sport catch would be:

```
    LegalSportCatch = \frac{QS*(1-mre)}{ZM}*CH_{M}*(1-exp^-ZM)         Eq. 4.42
```

The above equations would be applied for fish that could be retained.  For shaker mortalities, a corresponding set of equations, replacing base period exploitation rates for landed catch by base period shaker exploitation rates.

**Inconsistent Regulations Within a Time Period**

Regulations for a given fishery may change during a modeled time period.  For example, a fishery may operate under different retention or size limits during different time periods within a single time period used for cohort analysis.

**Mixed-Bag Limits**

Mixed-bag limits (multiple species, marked-unmarked, ratio fisheries) can affect estimation of incidental mortalities and catchability coefficients.  Methods to estimate mixed-bag effects have not yet been developed, but could involve Monte Carlo simulation techniques or other algorithms (Gazey, pers. com.).

# Chapter 5.  Cohort Reconstruction

Cohort reconstruction rebuilds the marine life history of a given CWT release group backwards, starting with the last time period for the oldest age class and ending with an estimate of the ocean the number of age 2 fish in the first time period.  The general sequence of run reconstruction is depicted in figure 5.1 below.

```mermaid
graph TD;
  Start - Stock, Brood-->Esc Prorated by Time Pd?;
  Esc Prorated by Time Pd?-->Total Fishing Morts for all time periods for Term Fisheries that can be quantified directly;
  Total Fishing Morts for all time periods for Term Fisheries that can be quantified directly-->Prorate Escapement in proportion to incidental mortalities;
  Esc Prorated by Time Pd?-->Y;
  Prorate Escapement in proportion to incidental mortalities-->Adjust Esc for Pre-Spawn Mortality;
  Y-->Adjust Esc for Pre-Spawn Mortality;
  Adjust Esc for Pre-Spawn Mortality-->Establish N at Mort Rate by age and time period;
  Establish N at Mort Rate by age and time period-->Initialize. Age=Oldest, Time=Last, Set up Big Time loop covering all ages and time periods. Cohort=0;
  Initialize. Age=Oldest, Time=Last, Set up Big Time loop covering all ages and time periods. Cohort=0-->Compute Direct Estimates of Catch & IM for Term Fisheries;
  Compute Direct Estimates of Catch & IM for Term Fisheries-->Computer HRs for IMs where necessary for Terminal Fisheries;
  Computer HRs for IMs where necessary for Terminal Fisheries-->Compute Mature Run Size & Estimate IMs for fisheries included in previous step;
  Compute Mature Run Size & Estimate IMs for fisheries included in previous step-->Compute MatRate & AEQ Factor;
  Compute MatRate & AEQ Factor-->Compute Direct Estimates of Catches & IM for PreTerm Fisheries;
  Compute Direct Estimates of Catches & IM for PreTerm Fisheries-->Compute ERs for IMs where necessary for PreTerm Fisheries;
  Compute ERs for IMs where necessary for PreTerm Fisheries-->Compute PreTerm Cohort & estimate IMs for fisheries included in previous step;
  Compute PreTerm Cohort & estimate IMs for fisheries included in previous step-->Apply Natural Mortality to Cohort;
  Apply Natural Mortality to Cohort-->Store Cohort, Mat Run, & Fishing Morts;
  Store Cohort, Mat Run, & Fishing Morts-->First Time Period?;
  First Time Period?-->N;
  N-->Compute Direct Estimates of Catch & IM for Term Fisheries;
  First Time Period?-->Y;
  Y-->Done;
```
Fig. 5.1.  General sequence of run reconstruction.

## Mature Run Size

The mature run size is comprised of the total estimated spawning escapement plus fishing mortalities in terminal fisheries during the time period of interest.  Two scenarios are considered: (1) Information is available to prorate spawning escapement; and (2) No information is available to prorate spawning escapement.  When information is available to prorate spawning escapement, the mature run size is comprised of the estimated spawning escapement plus fishing mortalities in terminal fisheries during the time period of interest.

```
    MatRun_{a,t} = Esc_{a,t}+\sum_{f=term}{(CWT_{f,a,t}+IMort_{f,a,t})} Eq. 5.1
```

Note that methods described earlier for CNR and MSFs generate estimates of terminal run harvest rates instead of mortalities.  In this situation, the mature run can still be generated using the following procedure:

Compute the total harvest rate on the terminal run as the sum of the harvest rates exerted during the CNR and MSFs that affect the cohort:

```
    y_{a}=\sum_{f=CNR,MSF}Q?_{a,f}                                      Eq. 5.2
```
Now the total mature run can be estimated as:

```
    MatRun_{a,t} = \frac{MatRun_{a,t}}{(1-y_{a})}                       Eq. 5.3
```

Once the Mature Run size is estimated, the components of the incidental mortalities represented in eq. 5.7 can be computed by simply multiplying the cohort size by their associated exploitation rates.

## Pre-Terminal Cohort Size

Two scenarios are considered: (1) The brood year has completed its life cycle and all information is available for analysis; and (2) The brood year has not completed its life cycle.

**Complete Broods**

```
    CH_{a,t} = MatRun_{a,t}+Cohrt_{a,t+1}+\sum_{f=Pr{e}Term}(CWT_{f,a,t}+IMort_{f,a,t})                                                            Eq. 5.4
```

**Incomplete Broods**

CA is normally performed when all data for a given cohort are available.  However, because of the need to generate timely information on fishery impacts, CA is sometimes performed earlier.  In this case, average maturity rates are used to estimate the size of the cohort remaining after the last time period of data encompassed in the CA.  If the CA were to be performed only using available data, cohort sizes would be underestimated and fishery exploitation rates would be biased high as a result. For incomplete broods, the fish remaining in the ocean in the last time period for which data are available is computed as:

```
    ACohrt_{a,t+1} = MatRun_{a,t}\left(\frac{\left(1-AvgMatRte_{a,t}\right)}{AvgMatRte_{a,t}}\right)                                               Eq. 5.5
```

The estimated Pre-Terminal cohort size prior to fishing is:

```
    CH_{a,t} = MatRun_{a,t}+ACohrt_{a,t+1}+\sum_{f=PreTerm}(CWT_{f,a,t}+IMort_{f,a,t})                                                             Eq. 5.6
```

Note that methods described earlier for CNR and MSFs generate estimates of exploitation rates instead of mortalities.  In this situation, the pre-terminal cohort size can still be generated using the following procedure:

Compute the total exploitation rate of the preterminal cohort as the sum of the exploitation rates exerted during the CNR and MSFs that affect the cohort:

```
    y_{a} = \sum_{f=CNR,MSF}Q?_{a,f}                                    Eq. 5.7
```

Now the total cohort size can be estimated as:

```
    CH_{a,t} = \frac{CH_{a,t}}{(1-y_{a})}                               Eq. 5.8
```

Once the total cohort size is estimated, the components of the incidental mortalities represented in eq. 5.7 can be computed by simply multiplying the cohort size by their associated exploitation rates.  All fishing mortality estimates for this time step should be stored at this point in the run reconstruction.

## Cohort Size Before Natural Mortalities

```
    Cohrt_{a,t}=\frac{CH_{a,t}}{SurvRte_{a,t}}                          Eq. 5.9
```

# Chapter 6.  Statistics for Exploitation Rate Analysis

## Fishery exploitation rates.

For purposes of exploitation rate analysis, fishery impacts of reported catch and incidental mortalities can be expressed in terms of exploitation rates, regardless of whether fisheries are considered terminal or pre-terminal.  This will simplify calculations and facilitate reporting since impacts within a single time period can be simply added together.  By definition, exploitation rates represent the proportion of the cohort alive at the start of the period that is accounted for by a particular process (e.g., reported catch, drop off, shaker, CNR) and fishery.

```
    ExRt_{a,f,t} = \frac{mort_{a,f,t}}{CH_{a,t}}                        Eq. 6.1
```

## Maturation Rate Computations

**Complete broods**
The maturation rate for the last time period and oldest age class =1.  For other time periods and age classes, maturation rates are computed as:

```
    MatRte_{by,a,t} = \frac{MatRun_{by,a,t}}{MatRun_{by,a,t}+Cohrt_{by,a,t+1}}            Eq. 6.2
```

**Incomplete broods**
For incomplete broods, it is necessary to estimate the maturation rate for the last time period and oldest age class differently[^9].

```
    MatRte_{by,A,T} = \frac{\sum_{by}MatRte_{by,A,T}}{NumComplBY}       Eq. 6.3
```

## Adult Equivalence Factors

Adult equivalence factors (AEQ) represent the probability that a fish alive at the start of a fishing period will return to its river of origin on its spawning migration in the current or any future period in the absence of fishing.  By definition, AEQs for terminal fisheries and for pre-terminal fisheries during the last time step for the oldest age in the cohort = 1.  For pre-terminal fisheries for other time steps and age classes, AEQs are computed as:

```
    AEQ_{by,a,t} = MatRte_{by,a,t}+(1-MatRte_{by,a,t})*SurvRte_{by,a,t+1}*AEQ_{by,a,t+1}                                                           Eq. 6.4
```

## Ocean Age 1 Survival Rate

CA methods can be used to estimate the ocean age 1 (pre-recruitment) survival rate by dividing the cohort size determined through reconstruction by the number of fish released for the CWT group.

```
    SurvRte_{by,1,1} = \frac{Cohrt_{by,1,1}}{Release_{by}}              Eq. 6.5
```

In most equations below, the cohort size is first reduced by the non-catch mortalities:

```
    CH_{by,i} = (Cohrt_{by,i})(SurvRte_{i})
```

(i) Calculation of Maturity Rate:

If f &lt; or &gt; terminal fishery then

```
    TotOcnCat_{by,i} = \sum_{f}\left[Catch_{f,by,i}+Shak_{f,by,i}+RL_{f,by,i}+RS_{f,by,i}\right]
```
(summed across all non-terminal fisheries)
If f = terminal fishery then

```
    TotMortCat_{by,i} = \sum_{f}\left[Catch_{f,by,i}+Shak_{f,by,i}+RL_{f,by,i}+RS_{f,by,i}\right]
```
(summed across all terminal fisheries)

```
    MatRun_{by,i} = TotMatCat_{by,i}+Escape_{by,i}
```
```
    MatRte_{by,i} = \frac{MatRun_{by,i}}{(Cohrt_{by,i})(SurvRte_i)-TotOcnCat_{by,i}}
```

(ii) Calculation of Adult Equivalents:

```
    AdltEqv_{by,i} = MatRte_{by,i}+\left[(1-MatRte_{by,i})(SurvRte_{i+1})(AdltEqv_{by,i})\right]
```
by definition:

```
    AdltEqv_{by,maxage} = 1
```

(iii) Calculation of Average Maturity Rate and Average Adult Equivalents:

```
    AvgAdltEqv_{i} = \frac{\sum_{by}{AdltEqv_{by,i}}}{NumComplBY}
```
(summed across all complete brood years)

(iv) Calculation of Estimated Cohort (for Incomplete Brood Years only):

We can express the maturity rate in equation (6) as follows:

```
    MatRte_{i} = \frac{MatRun_{by,i}}{MatRun_{by,i}+Cohrt_{by,i+1}}
```

If we solve the above equation for _Cohrt__by,i+1_ and use average maturity rates in place of the actual maturity rate, we obtain:

```
    ACohrt_{by,iage+1} = \left(1-AvgMatRte_{iage}\right)\left(\frac{MatRun_{by,iage}}{AvgMatRte_{iage}}\right)
```

This estimated cohort can then be incorporated into the equation (i) as Cohort _i_+1 to correct for the bias introduced in any calculations using cohort size in incomplete brood years (e.g., shaker weights, exploitation rates, etc…).

For any age _i_, the estimated cohort can be reduced by the non-catch mortality rate:

```
    ACH_{by,i} = \left(ACohrt_{by,i}\right)\left(SurvRte_i\right)
```

(v) Calculation of Age Specific Ocean Exploitation Rate:

If BY = Complete BY (all ages present to MaxAge) then

```
    OcnEXR_{by,i} = \frac{TotOcnCat_{by,i}}{CH_{by,i}}
```

elseif BY = Incomplete BY then

```
    OcnEXR_{by,i} = \frac{TotOcnCat_{by,i}}{ACH_{by,i}}
```


### Calculation of Incidental Mortalities (Legal Catch):

(i) Total Population for all Calculations:

If f &lt; or &gt; Terminal Fishery then

 if BY = Complete BY (all ages present to MaxAge) then

```
    ShakPop_{f,by,i}=CH_{by,i}
```

elseif BY = Incomplete BY then

```
    ShakPop_{f,by,i}=ACH_{by,i}
```

elseif _f_ = Terminal Fishery then

 if BY = Complete BY (all ages present to MaxAge) then

```
    ShakPop_{f,by,i}=MatRun_{by,i}
```

elseif BY = Incomplete BY then

```
    ShakPop_{f,by,i}=\left(ACH_{by,i}\right)\left(1-OcnEXR_{by,i}\right)\left(AvgMatRte_{i}\right)
```

(ii) Calculation of Population of Non-Vulnerable Fish:

```
    NNV_{f,by,i}=\left(CH_{f,by,i}\right)\left(1-PV_{f,by,i}\right)
```

If calendar year method used then

```
    NNV_{f,by,.}=\sum_{i}{NNV_{f,by=yr-t,i}}
```

elseif brood year method used then

```
    NNV_{f,by,.}=\sum_{i}{NNV_{f,by,i}}
```
(summed across all years)

(iii) Calculation of Population of Vulnerable Fish:

```
    NV_{f,by,i}=\left(ShakPop_{f,by,i}\right)\left(PV_{f,by,i}\right)
```

If calendar year method used then

```
    NV_{f,by,.}=\sum_{i}{NV_{f,by=yr-t,i}}
```

elseif brood year method used then

```
    NV_{f,by,.}=\sum_{i}{NV_{f,by,i}}
```

(summed across all years)

(iv) Estimated Encounter Rate:

```
    ER_{f,xx}=\frac{NNV_{f,xx,.}}{NV_{f,xx,.}}
```

where xx = by or xx = yr, depending on estimation method chosen

(v) Estimated Shaker Loss for all ages:

```
    Shak_{f,xx}=SMS_f\left(Catch_{f,xx,.}\right)\left(ER_{f,xx}\right)
```

where xx = by or xx = yr, depending on estimation method chosen

(vi) Shaker Loss in Age _i_ for fishery _f_:

If calendar year method used then

```
    Shak_{f,by=yr-i,i}=Shak_{f,yr,.}\left(\frac{NNV_{f,by=yr-i,i}}{NNV_{f,yr,.}}\right)
```

However, we know from Equations 20-22 that _Shak __f,yr,_. Also contains the term _NNV_ f,yr,_.  Therefore, this term can be cancelled out and the above equation simplifies to:

```
    Shak_{f,by=yr-i,i}=\left(Catch_{f,yr,.}\right)\left(SMS_f\right)\left(\frac{NNV_{f,by=yr-i,i}}{NV_{f,yr,.}}\right)
```

Therefore, it is not necessary to actually calculate the encounter rates and the total number non-vulnerable.

Elseif brood year method used then

```
    Shak_{f,by,i}=\left(Catch_{f,br,.}\right)\left(SMS_f\right)\left(\frac{NNV_{f,by,i}}{NV_{f,yr,.}}\right)
```

(by similar reasoning as for the calendar year method equation).

### Calculation of CNR mortalities in Exploitation Rate Analysis:

Mortalities caused by the CNR fisheries can be estimated by two methods, depending on the type of data available:

a) The preferred method uses an independent estimate (usually from sampling) of the encounters of legal and sub-legal fish during the fishing year in question; or,
b) In the absence of sampling information, the alternate method calculates a relative ratio of the legal to CNR season length to estimate the CNR mortalities.

(i) Calculation of Legal CNR Catch, fishery _f_ (LCNR known):

```
    RL_{f,by=yr-i,i}=\left(Catch_{f,br=yr-i,i}\right)\left(SML_f\right)\left(\frac{LCNR_{f,yr}}{L_{f,yr}}\right)
```

(ii) Calculation of Sub-Legal CNR Catch, fishery _f_ (SLCNR known):

```
    RE_{f,yr,.}=\left(\frac{LCNR_{f,yr}}{L_{f,yr}}\right)\sum_{i}{Catch_{f,by=yr-i,i}}
```

(summed across all ages)

The term (_LCNR __f,yr__ /L__f,yr_) is a constant and is therefore removed from the summation term.

```
    RS_{f,yr,.}=\left(\frac{RE_{f,yr,.}}{LCNR_{f,yr}}\right)\left(SLCNR_{f,yr}\right)\left(SMS_f\right)
```

But we know that _RE __f,yr,._ Also contains the value _LCNR_ f,yr_ .  This value cancels out and the equation simplifies to:

```
    RS_{f,yr,.}=\left(SLCNR_{f,yr}\right)\left(SMS_f\right)\left(\frac{\sum_{i}{Catch_{f,by=yr-i,i}}}{L_{f,yr}}\right)
```

(summed across all ages)

```
    RS_{f,by=yr-i,i}=\left(RS_{f,yr,.}\right)\left(\frac{Shak_{f,by=yr-i,i}}{Shak_{f,yr,.}}\right)
```

(This means that the legal encounters do not have to be summed across all ages before calculating the sub-legals.)

(iii) Calculation of Legal CNR Catch, fishery _f_ (LCNR unknown):

```
    RL_{f,by=yr-i,i}=\left(Catch_{f,br=yr-i,i}\right)\left(\frac{SeaCNR_{f,yr}}{SeaL_{f,yr}}\right)\left(SML_f\right)SelLCNR_f
```

(iv) Calculation of Sub-Legal CNR Catch, fishery _f_ (SLCNR unknown):

```
    RS_{f,by=yr-i,i}=\left(Shak_{f,br=yr-i,i}\right)\left(\frac{SeaCNR_{f,yr}}{SeaL_{f,yr}}\right)\left(SML_f\right)SelLCNR_f
```

### Calculation of Age Specific Fishery Exploitation Rates:

(i) Determination of Total Population for all Calculations:

If f &lt; or &gt; Terminal Fishery then

 if BY = Complete BY (all ages present to MaxAge) then

```
    EXRPop_{f,by,i}=CH_{by,i}
```
 
 elseif BY = Incomplete BY then

```
    EXRPop_{f,by,i}=ACH_{by,i}
```

 elseif f = Teminal Fishery then

```
    EXRPop_{f,by,i}=MatRun_{by,i}
```

(ii) Calculation of Fishery Exploitation Rates:

for legal catch exploitation rate:

```
    EXRLeg_{f,by,i}=Catch_{f,by,i}\left(\frac{AdltEqv_{by,i}}{EXRPop_{f,by,i}}\right)
```

for total mortality exploitation rate:

```
    TotCatch_{f,by,i}=Catch_{f,by,i}+Shak_{f,by,i}+RL_{f,by,i}+RS_{f,by,i}
```

```
    EXRTot_{f,by,i}=TotCat_{f,by,i}\left(\frac{AdltEqv_{by,i}}{EXRPop_{f,by,i}}\right)
```

## Uncertainty Surrounding Estimates of Exploitation Rates By Strata

See Bernard &amp; Clark stuff.



# Chapter 7.  Exploitation Rate Analysis

The CTC produces an annual &quot;Exploitation Rate Analysis&quot; (ERA) based on cohort analysis for stocks with suitable available data.  The ERA generates the following set of statistics:

- Estimates of brood year exploitation rates for reported catch and total mortalities
- Estimates of the distribution of mortalities among fishing regions by calendar year
- Estimates of ocean age 1 survival rates
- Estimates of fishery indices

**Brood Year Exploitation Rates**

The brood year exploitation rates reflect fishery impacts in terms of AEQs over the life of the brood.  The impact may be computed in either landed (actual CWT recoveries) catch or total fishing mortalities (actual CWT recoveries plus imputed fishing mortalities).  The landed catch brood year exploitation rates are computed as:

```
    BYERC_{by}=\frac{\sum_{a=1}^{A}\sum_{t=1}^{T}\sum_{f=1}^{F}{CWT_{by,a,t,f}*AEQ_{by,a,t,f}}}{\sum_{a=1}^{A}\sum_{t=1}^{T}\sum_{f=1}^{F}{(CWT_{by,a,t,f}+IMort_{by,a,t,f})*AEQ_{by,a,t,f}+\sum_{t=1}^{T}\sum_{a=1}^{A}{Esc_{by,a,t}}}}                                                                 Eq. 7.1
```

Total fishing mortality brood year exploitation rates are computed as:
```
    BYERC_{by}=\frac{\sum_{a=1}^{A}\sum_{t=1}^{T}\sum_{f=1}^{F}{(CWT_{by,a,t,f}+IMort_{by,a,t,f})*AEQ_{by,a,t,f}}}{\sum_{a=1}^{A}\sum_{t=1}^{T}\sum_{f=1}^{F}{(CWT_{by,a,t,f}+IMort_{by,a,t,f})*AEQ_{by,a,t,f}+\sum_{t=1}^{T}\sum_{a=1}^{A}{Esc_{by,a,t}}}}                                              Eq. 7.2
```
 
**Distribution of mortalities**

The distribution of fishing mortalities provides information on the proportion of AEQ fishing mortalities for a given stock are accounted for by fisheries in particular regions within a single calendar year.  Consequently, these statistics combine results of CA on individual broods.  Distribution statistics can be computed for either landed catch or total fishing mortalities.  For landed catch, the proportion of fishing mortalities accounted for by a particular set of fisheries is computed as:

```
    DistC_{y,fg}=\frac{\sum_{f=fg}\sum_{a=1}^{A}\sum_{t=1}^{T}{CWT_{y-a,a,t,f}*AEQ_{y-a,a,t,f}}}{\sum_{f=1}^{F}\sum_{a=1}^{A}\sum_{t=1}^{T}{CWT_{y-a,a,t,f}*AEQ_{y-a,a,t,f}}}                                                 Eq. 7.3
```

For total fishing mortality:

```
    DistT_{y,fg}=\frac{\sum_{f=fg}\sum_{a=1}^{A}\sum_{t=1}^{T}{(CWT_{y-a,a,t,f}+IMort_{y-a,a,t,f})*AEQ_{y-a,a,t,f}}}{\sum_{f=1}^{F}\sum_{a=1}^{A}\sum_{t=1}^{T}{(CWT_{y-a,a,t,f}+IMort_{y-a,a,t,f})*AEQ_{y-a,a,t,f}}}         Eq. 7.4
```

**Estimates of Ocean Age 1 Survival Rates**

Already done

**Fishery Indices (amend to include SPFI)**

The 1985 PST established a rebuilding program that relied upon the progressive reduction of brood year exploitation rates over time.  The regulatory mechanism adopted by the 1985 PST was designed to reduce brood year exploitation rates by reducing harvest rates by fisheries under fixed ceiling management over time as stocks rebuild.  That is, harvest rates were expected to decline as stocks rebuilt and abundance increased by maintaining catch levels for highly mixed stock fisheries at fixed levels.

Changes in brood year exploitation rates can best done through CA on complete brood year data.  However, the fisheries operating under PST ceilings exploit complex mixtures of stocks and ages.  If only data for complete broods could be used, then it would not be possible to estimate a fishery harvest rate until several years after a fishery has occurred when data for all impacted broods become available.  This would eliminate any possibility of adjustments to management regimes in response to rebuilding progress towards achieving desired brood year exploitation rates on depressed stocks.

The CTC is tasked with the responsibility of monitoring and reporting changes in fishery harvest rates under PST regimes.  Methods capable of directly estimating fishery harvest rates have not been developed because the CTC has been unable to determine the proportion of each stock and age that are available to a given fishery.

To reflect changes in fishery harvest rates, the CTC has devised a fishery index that reflects impacts relative to a selected base period.  This method utilizes the stock-age-fishery specific exploitation rates derived through CA.  This method provides an early indication of changes in fishery harvest rates which can be taken into consideration when assessing the effectiveness of management measures in achieving Chinook conservation goals.

In theory, a fishery should exert the same harvest rate on all vulnerable populations.  The CA procedures produce stock-age specific estimates of fishery exploitation rates, which of course will reflect differences in the distribution and exploitation patterns that are characteristic of individual stocks.

*Assumptions:*

The various assumptions which underlie the estimate have already been discussed (2.1.2.4).

*Methods:*

A fishery index reflects changes in the impact of a fishery upon a stock or a group of stocks over time.  For a given fishery, there will be several stock specific estimates of an exploitation rate for the base period and for the current year generated through CA.

In a 1988, Jim Scott employed simulation modeling to evaluate four methods of combining the stock specific CA estimates to produce the best estimate of the change in the fishery harvest rate: (A) Simple Average; (B) Ratio of Means; (C) Variance; and (D) Weight by Exploitation Rate.  A discussion of each of the four methods follows (Note that the calculations presented DO NOT REFLECT multiple time steps, but rather Scott&#39;s original analysis).  Note that the fishery indices can be calculated in terms of landed catch or total fishing mortalities.  In the following discussion, if the fishery index reflects total fishing mortalities,

```
    EXRA_{f,s,yr,a}=EXRTot_{f,yr-a,a}
```

If the fishery index is intended to reflect impacts of only landed catch,
```
    EXRA_{f,s,yr,a}=EXRLeg_{f,yr-a,a}
```

## Simple Average Method

This method calculates an unweighted mean of the ratios of each stock specific exploitation rate to the base period exploitation rate.  First, calculate the base period average exploitation rate for a stock and age:

```
    BEXR_{f,s,a}=\sum_{yr=1979}^{1982}\frac{EXRA_{f,s,yr,a}}{n}         Eq. 7.5
```

where _n_ = number of years with data in the base period average.

Next, calculate an exploitation rate index for each stock and age:

```
    NEXR_{f,yr,a}=\frac{EXRA_{f,s,yr,a}}{BEXR_{f,s,a}}                 Eq. 7.6
```

Finally, calculate the unweighted mean of exploitation rate indices (over all stocks present in each year):

```
    SAvgExRt_{f,yr}=\frac{1}{n}\sum_{s=i}^{n}\sum_{a=1}^{A}{NEXR_{f,s,yr,a}}                                                                       Eq. 7.7
```

where: _n_ = number of stock-age combinations considered in a fishery.

## Ratio of Means Method

This method calculates a weighted mean of the ratios of the current stock specific exploitation rates to the sum of average base period exploitation rates (over all stocks).

```
  RatioEXR_{f,yr,a}=\frac{\sum_{s=1}^{S}\sum_{a=1}^{A}{EXRA_{f,s,yr,a}}}{\sum_{s=i}^{S}\sum_{a=1}^{A}{BEXR_{f,s,a}}}                                 Eq. 7.8
```

## Variance Method

This method weights by the inverse of the variance between tag codes within a stock.  A variance Var(_NEXR__f,s,yr,i_) was computed for all stocks with more than one tag group per brood year.  If only one tag group was present, then average variance for the stock, age, and fishery was used.

```
    IVAR_{f,yr,a}=\frac{\sum_{s}\frac{NEXR_{f,s,yr,a}}{Var(NEXR_{f,s,yr,a})}}{\sum_{s}\frac{1}{Var(NEXR_{f,s,yr,a})}}          Eq. 7.9
```

The variance was estimated as:  (INSERT EQUATION)

## Weight by Exploitation Rate Method

This method calculates the exploitation rate index weighted by the exploitation rate.

```
    EXRwEXR_{f,yr,a}=\frac{\sum_{s}\frac{EXR{A_{f,s,yr,a}}^2}{BEXRs,_{f,a}}}{\sum_{s}{EXRA_{f,s,yr,a}}}                                           Eq. 7.10
```

   ?????? this is the original, but it's not correct

The cohort size at any age is calculated by summing all catches at age (including incidental mortalities), the escapement at age, and the cohort from the next older age.  (Equation 1 below).

A detailed description of the methods the used in calculating and combining the indices is presented later in this Appendix.


#### Calculation of Exploitation Rate Indices

Note: In the following discussion, the variable EXRA _f,s,yr,i_ is used to identify the fishery and age specific exploitation rate by stock which is the output of the cohort analysis.

The above assignments relate the previous discussion concerning the cohort analysis of a single stock over all discussion of linking the calculated exploitation rates of all stocks available for each fishery.
**Selection of Methods:**

The simple average index was the method used in the 1986 and 1987 Exploitation Rate Analysis.  This method was continued this year to provide continuity with previous analyses.  The Ratio of the Means Index yielded the smallest variance and Mean Square Error of the four estimators evaluated (Scott, 1988).  For this reason, the ratio method was also chosen for this year&#39;s analysis.  The other two methods yielded results which were either equal to or inferior to the two methods chosen.

For the 1988 analysis, exploitation rates were estimated using legal catch only and using total fishing mortality (legal catch + sub legal mortality + all CNR mortality).

The average exploitation rate index during the base period will be 1 (one).  Therefore, a fishery exploitation rate index less than one represents a decrease from the base period while a fishery exploitation rate index greater than one indicates an increase.  The magnitude of the change will simply be the difference of the measured exploitation rate index from one.

Selection of Stocks and Ages to Include in Fishery Index Calculations

**Assessing Change in the Exploitation Rate Indices**

Age specific exploitation rate indices were calculated for each fishery of interest over all the stocks present.  Then the percent change of the exploitation rate index from the base period was calculated:

    (52) EXRC_{f,yr,i}=100*\left(SAVEXR_{f,yr,i}-1\right)

    (53) EXRC_{f,yr,i}=100*(RatioEXR_{f,yr,i}-1) 

In addition, the percent change in a fishery exploitation rate index was averaged for 1985, 1986, and 1987 seasons to estimate short term trends.

The objective of these analyses was to compare the observed exploitation rate changes to the expected reductions for each fishery of interest.  Given the fact that size limit changes have been implemented and CNR fisheries have increased from the base period, the appropriate exploitation rate measurement fro comparison to expected reductions is the one calculated using total mortality (legal catch + sublegal mortality + CNR mortality).

This analysis is only applicable to the initial years of the rebuilding program (first cycle). Assuming that the abundance in a fishery remains constant, the expected reduction in a fishery exploitation rate is directly proportional to the reduction in catch from the base period to the PSC ceiling. Therefore, the following relationship between the reduction in a catch ceiling and the expected fishery exploitation rate is made:

    (54) EEXR_f=\left(\frac{PSC_f}{BPC_f}\right)BEXR_f

The expected percent reduction in the exploitation rate, for a fishery of interest, is calculated as follows:

    (55) EPR_f=100*\left(\frac{1-PSC_f}{BPC_f}\right)

Inferences about changes in abundance could be made if observed exploitation rate reductions greatly deviate from expected reductions, and the other assumptions about cohort analysis are met (Starr et al., 1986).

# Chapter 8.  Statistics for the CTC Model

Part I – Base Period Data Available

Part II – Base Period Data Unavailable (Out of Base Adjustments)



# APPENDIX A

Summary of Changes to Cohort Analysis Procedures

Topic:

With implementation of fishery constraints under the PSC Agreements, the conduct of fisheries has been changing.  These changes require methods to evaluate stock-age specific impacts due to differences in growth, migration, and exploitation patterns.

Cohort Analysis (CA) lies at the core of the stock and fishery assessment methods employed by the Chinook Technical Committee (CTC).  Estimates of fishery impacts produced by CA provide information to monitor trends in stock-specific patterns of fishery exploitation and survival, impacts of individual fisheries, and parameters for simulation modeling.

The current Cohort Analysis program (COHSHK11.BAS) must be modified to produce parameter estimates that will be required to accommodate several desired improvements to the Chinook Technical Committee&#39;s (CTC) model and to generate statistics for fishery impact assessments required under agreements of the Pacific Salmon Commission (PSC).  The purpose of this memo is to describe current methods and proposed changes for cohort analysis.

The vast majority of the proposed changes to Cohort Analysis result from the CTC&#39;s interest in expanding the number of time steps within a single season.  Other changes are due to regulations such as total Chinook non-retention, mark-selective fisheries, and modification of size limits.



Summary of Proposed Changes:

| Topic | Current | Proposed |
| --- | --- | --- |
| Program Structure | Several programs must be run to generate desired outputs | Integrate programs to: (1) perform cohort analysis; (b) generate CTC model input parameters, including out of base adjustments. |

## Stratification
| Topic | Current | Proposed |
| --- | --- | --- |
|Time Strata | Annual | User-specified number of time steps within a year. |
|Fishery Strata | User-defined.  Fisheries designated as terminal or preterminal, depending on stock and age for the entire year. Fish harvested by fisheries occurring near its river of origin are usually designated as terminal for a given stock, regardless of age.  For net fisheries outside the vicinity of a stock&#39;s river of origin, all fish age 4 and older are considered to be mature; consequently, for fish that are destined to spawn in other areas, net fisheries can be considered to be preterminal for fish younger than age 3 and terminal for fish older than age 3. | User defined.  Fisheries will need to be designated as being preterminal or terminal by stock and age for each time step. |
| Stock Strata | CWT-based | No change for stocks with CWTs.  Perform separate Cohort Analysis for natural stocks, based on recoveries of associated releases of CWT indicators. |

## Natural Mortalities
| Topic | Current | Proposed |
| --- | --- | --- |
| Apportionment | All natural mortality occurs at the start of a year.  Assumes the following annual natural mortality schedule:| There are a variety of methods that can be employed, including: (a) apply natural mortality annually when the fish are aged; (b) apportion annual mortality according to the lengths of the time steps employed in the CTC Model; or (c) apportion annual mortality by the mean length of fish at the mid-point of the time steps employed in the CTC model.|

| Natural Mortality | Ocean Age |
| --- | --- |
| 0.5 | 1 |
| 0.4 | 2 |
| 0.3 | 3 |
| 0.2 | 4 |
| 0.1 | 5+ |

## Observed Recoveries
| Topic | Current | Proposed |
| --- | --- | --- |
| Escapements | Performed on an annual time step with all escapements occurring at the end of the year. | Partition escapement estimates to appropriate time strata. Generally, only a single escapement estimate is available. For the most part, data to partition escapements are not available, although there are a few instances where escapement estimates are available by time period (e.g., dam counts on the Columbia River). |
| Landed Catch | Allocated to single fishery strata | Allocate CWT recoveries and landed catches to fishery-time strata corresponding to those employed in the CTC Model. |

## Imputed Recoveries
| Topic | Current | Proposed |
| --- | --- | --- |
| Pre-Spawning Mortality.  For some stocks, significant mortality occurs between the last fishery and spawning escapement.  These prespawning mortality rates are termed IDLs (Interdam Losses) | Stock specific | Stock-age specific for each time strata. |
| Shaker Mortality.  Incidental mortalities of fish below the minimum legal retention size limit for a fishery are termed &quot;shaker loss.&quot;  The proportion of a given cohort (ocean age class) below the minimum size limit is termed the PNV for &quot;Proportion Non-Vulnerable&quot; (PNV) to the gear used in the fishery. | Assumes a single size distribution for fish of a given age for a specific fishery.  These distributions were derived biological sampling of troll and seine caught fish containing CWTs during the late 1970&#39;s-early 1980&#39;s.  Use of this method implicitly assumes that fish from a given stock and age have different size distributions, depending on where they are caught.   | Compute PNVs using stock-specific growth functions.  Stock-specific size (length) distributions will be characterized by means and variances, assuming Normal Distributions.  Compute PNVs through the use of approximation to cumulative distribution functions. |
| Drop Off.  Drop Off mortalities account for incidental mortalities of fish that are killed as a result of encountering the gear, but do not appear in the reported catch.  Examples of such losses include loss to predators like sharks or marine mammals, escape with lethal wounds, or fish taken for personal use.  Data from research studies which can be used to quantify drop off mortalities are scarce.   | assumed proportion of fish brought to the boat, inclusive of legal and sublegal-sized fish. | No change |

## Regulatory Measures
| Topic | Current | Proposed |
| --- | --- | --- |
| Size Limits | A single size limit applies to a given fishery for the entire year | Capacity to incorporate specific size limits for fisheries by time strata.  There is a need to estimate impacts where: (a) more than one size limit may apply during a given time period; (b) slot limits are employed which permit retention of only fish between specified minimum and maximum sizes; and (c) gear-effects (e.g., large-mesh gill nets or large plugs tend to reduce encounters of smaller fish, even though minimum size limits may not be changed). |
| Mixed Bag Limits (multiple species, marked-unmarked, ratio fisheries) can affect estimation of incidental mortalities and catchability coefficients.   | Not considered. | Incorporate methods to estimate mixed-bag effects.  Methods have not yet been developed, but could involve Monte Carlo simulation techniques or parameterization (Gazey, pers. Comm.). |
Non-Retention (CNR).  With the institution of constraints on landed catch under PST regimes, managers have frequently resorted to non-retention restrictions to access harvestable fish from other species. 
| Topic | Current | Proposed |
| --- | --- | --- |
|   Selectivity factors are employed to represent the relative change in Chinook encounter rates resulting from gear restrictions or fishing methods when fisheries are directed at other species.  Rarely are selectivity factors supported with observational data, although estimates could be derived from historical catch and effort data. | Simple user-specified scalar value for a fishery to be applied for entire season. | Simple user-specified scalar value for a fishery to be applied for time period |

## CNR With Landed Catch  
Chinook non-retention restrictions have been most commonly employed during only a portion of the season, once allowable limits on landed catch have been reached.  Four general methods are currently incorporated into cohort analysis procedures, depending on the availability of observational data.

| Topic | Current | Proposed |
| --- | --- | --- | 
| RT Method. | No observational data are employed.  Instead, non-retention mortalities are estimated through a CTC model-generated RT factor (RT is an abbreviation for &quot;ratio&quot;, since it represents the ratio between the landed catch and the catch that would have resulted under base period conditions (i.e., base period exploitation/harvest rates). | Convert (1-RT)\*BPER to an instantaneous rate and apply by time strata. |
| *Season Length* Under this CNR method, | observational data consist of the number of days the fishery is open to Chinook retention and the number of days that non-retention restrictions were in effect. | No change, except applied by time strata. |
| *Effort* | observational data consist of the effort during the period when the fishery was open to Chinook retention and the effort during the non-retention period. | No change, except applied by time strata. |
| *Reported Encounters.* | observational data consist of the number of Chinook landed, the number of legal size Chinook encountered during the non-retention period, and the number of sublegal size Chinook encountered during the non-retention period.  Encounter data are not stock-specific. | No change, except applied by time strata.  Monitoring programs may need to be designed to generate estimates by time period employed in cohort analysis. |
| No Landed Catch.  In recent years, Chinook retention has been prohibited entirely in some fisheries.  Some of the fish released during non-retention fisheries are expected to survive and be vulnerable to recapture.  The potential for multiple encounters of the same fish is evaluated by converting the discrete linear catch equations normally used for cohort analysis for retention fisheries to continuous form. | Estimate encounters by computing catchability coefficients from historical estimates of exploitation rates of landed catch, assuming each fishery operates independently of the others.  These catchability coefficients are then used to estimate the number of encounters in the nonretention fishery.  Fish encountered in non-retention fisheries are subject to drop off and release mortality. | Estimate encounters by computing catchability coefficients from historical estimates of exploitation rates of landed catch, assuming fisheries interact with each other (competing exponential risk). |
| *Mark-Selective Fisheries.*  Mortalities associated with mark-selective fisheries (MSFs) have been conducted in certain terminal areas and may soon be implemented in pre-terminal areas.  These fisheries differ from non-retention fisheries in that fish with adipose fin clips are subjected to different mortalities than fish without adipose fin clips.  CWT recoveries of fish with clipped adipose fins in MSF can be analyzed in the same manner as with non-selective fisheries, except for the consideration of mark-retention error (see below). | Not addressed. | Since an important use of CWT data is to make inferences regarding fishery impacts on unmarked fish (predominantly naturally-produced fish), some means is necessary to use CWT recoveries to generate estimates of mortalities on unmarked fish. There are two basic approaches that can be employed to generate such estimates: (1) DIT; and (2) Assumption-Based Methods. **DIT** The PSC Selective Fishery Evaluation Committee (SFEC) has attempted to develop methods to try to provide stock-age-fishery estimates of exploitation rates on unmarked fish using Double Index Tagging (DIT) and Electronic Tag Detection (ETD).  In DIT, CWT groups are released in pairs that are supposed to be identical, except that adipose fins are removed from one group and left intact in the other.  ETD is required as a means to detect CWTs in fish with missing adipose fins and fish from DIT groups with adipose fins intact.  Differences in CWT recovery patterns can theoretically provide a means of estimating impacts of MSFs on the unmarked DIT group.  So far, only the &quot;paired ratio&quot; method appears to mold promise as a means of estimating stock-age-fishery specific impacts.  The capacity of the DIT/ETD approach to generate reliable stock-age-fishery specific impacts has not been verified.   **Assumption-Based Methods** In many instances, impacts of MSFs are likely too small to enable reliable quantification via DIT.  Under such circumstances, assumption-based methods will likely be required.  These methods rely primarily upon four principal parameters: (1) drop off; (2) release mortality rates; (3) mark-retention error; and (4) unmarked recognition error.   Drop off has already been discussed. **Release Mortalities** Immediate and delayed mortality rates of fish released after capture are required by fishery strata; different rates may be applied for legal and sublegal-sized fish. **Mark-Retention Error** Mark-retention error rates are expressed as the probability of a fish with a missing adipose fin being released.  Released fish are of course subject to release mortality rates. **Unmarked Recognition Error** Unmarked recognition error rates are expressed as the probability of a fish with an adipose fin being retained. **Multiple Encounters** An additional consideration is the potential for released fish to survive and be subject to recapture.  Some of the fish released during non-retention fisheries are expected to survive and be vulnerable to recapture.  The potential for multiple encounters of the same fish is evaluated by converting the discrete linear catch equations normally used for cohort analysis for retention fisheries to continuous form.  Currently, cohort analysis procedures estimate encounters by computing catchability coefficients from historical estimates of exploitation rates of landed catch.  These catchability coefficients are then used to estimate the number of encounters in the MSF fishery.  Fish encountered in non-retention fisheries are subject to drop off and release mortality. |
| *Inconsistent Regulations Within a Time Period:*  Regulations for a given fishery may change during a modeled time period.  For example, a fishery may operate under different retention or size limits during different time periods within a single time period used for cohort analysis.   | Not addressed. | Not addressed within a time period. |
| *Calculation of incidental fishing mortalities* | Shaker calculations can be done by one of two methods: brood year (i.e., use the accumulated catches for a total brood year, summed across all calendar years the brood year is in the ocean, to estimate the shakers for that brood year); or, calendar year (i.e., use the accumulated catches in a calendar year, summed over the brood years present, to estimate the shakers in that calendar year). | Brood Year basis only |
|
## Aging
| Topic | Current | Proposed |
| --- | --- | --- |
| <!-- --> | Fish are aged beginning January 1 | Age fish recovered in marine areas on October 1 for fall-type stocks, July 1 for spring type stocks. |

## Output Formats
| Topic | Current | Proposed |
| --- | --- | --- |
| <!-->--> | Formatted reports | Results placed in DataBase in two forms (1) results for exploitation rate analysis; (b) results for modeling purposes |

# APPENDIX B
**Notation**

Subscripts:

| Notation | Description |
| --- | --- |
| a | Age |
| A | Maximum Age |
| by | Brood Year |
| f | Fishery |
| s | Stock |
| t | Time Period |
| T | Last Time Period |
| y | Year |

Variable Definitions

|   |   |
| --- | --- |
| _ACH__by,a_ | Cohort size (discounted by natural mortality and for incomplete brood years only). |
| _ACohrt__by,a_ | Cohort size (for incomplete brood years only). |
| _AdltEqv__by,a_ | Adult Equivalent factor . |
| _AvgAdltEqv__a_ | Average equivalent factor_._ |
| _AvgMatRte__a_ | Average maturity rate. |
| _BPC__i_ | Catch in a fishery for the base period. |
| _BPER__f,s,a_ | Average base period exploitation rate |
| _BPAEQ__s,a_ | Average base period adult equivalent factor_._ |
| _Catch__f,,by,a_ | Reported Catch_._ |
| _Catch__f,xx_ | Total reported catch, where _xx_ = calendar year, added across all brood years present or _xx_ = brood year, added across calendar years. |
| CNREffort | Reported effort during Chinook Non-Retention periods |
| CNRS | Mortalities of Sub-Legal Sized fish during Chinook Non-Retention |
| CNRSeas | Length of Chinook Non-Retention Season |
| CNRL | Mortalities of Legal-Sized fish during Chinook Non-Retention |
| _Cohrt__by,a_ | Cohort size |
| _CH__by,a_ | Cohort size (discounted by natural mortality)_._ |
| _CWT__a,f,t_ | Estimated CWT Recoveries |
| _dmr__f_ | Drop Off Mortality Rate |
|   |   |
| _DO__f,t_ | Drop Off Mortality |
| _EEXR__f_ | Fishery exploitation rate expected under catch ceiling management_._ |
| encS | Encounters of Sub-Legal sized Fish (i.e., fish that are smaller than the minimum size limit that can be legally retained). |
| encE | Encounters of Extra-Legal sized fish (i.e., fish larger than the maximum size limit that can be legally retained). |
| _EPR__i_ | Expected percent change in the relative fishery exploitation rate from the base period average. |
| _ER__f,xx_ | Encounter rate for fishery, where _xx_ = calendar year or _xx_ = brood year. |
| _EscCWT__by,a_ | CWT Recoveries in Spawning Escapement |
| _Escape__by,a_ | Spawning Escapement. |
| _ExRt__f,s,y,a_ | Exploitation rate. |
| _ExRC__f,y_ | Percent change in exploitation rate. |
| _ExRLeg__f,by,a_ | Age Specific Exploitation Rate for reported catch only. |
| _ExRTot__f,by,a_ | Age Specific, Total Mortality Exploitation Rate |
| _EXRPop__f,by,a_ | Population from which the age specific exploitation rate is calculated_._ |
| _EXRwEXR__f,yr,i_ | Exploitation rate weight index. |
| FP | &quot;Fisheries Policy&quot; scalar to reflect stock-age specific changes to base period exploitation/harvest rates used in the CTC Model. |
| iage | Oldest age in an incomplete brood year. |
| _IDL__y,a_ | Pre-Spawning Mortality Rate |
| IllegalSportCatch | Fish that are unmarked, but legally retained in a Mark Selective Fishery |
| _imL__a_ | Incidental Mortality of Legal Sized Fish Due to Size Limits during retention fishery |
| _Imort__by,a,f,t_ | Total incidental fishing mortality |
| _imS__a_ | Incidental Mortality of Sub-Legal Sized Fish Due to Size Limits during retention fishery |
| _IVAR__f,by,a_ | Variance method index_._ |
| _L__f,y_ | Actual (total pieces) Landed Catch, pre-CNR (Chinook Non-Retention Fishery)_._ |
| _LCNR__f,y_ | Encounters of legal-sized fish during CNR.  Externally provided by agency | 
| LegalSportCatch | Marked Fish that are retained during a Mark Selective Fishery
| _MatRte__by,a_ | Maturity Rate. |
| _MatRun__by,a_ | Mature Run size. |
| maxSL_{y,f,t} | Maximum size limit |
| minSL_{y,f,t} | Minimum size limit |
| _minV__y,f,t_ | Minimum size for vulnerability to fishery |
| _mort__a,f,t_ | Mortality (e.g., reported catch, shakers) |
| ND(len) | Function that returns the proportion of a N(0,1) distribution that is below len. |
| _NEXR__f,s,y,a_ | Exploitation rate index |
| _NNV__f,by,a_ | Population not vulnerable to fishery. |
| _NNV__f,xx,._ | Total population not vulnerable to fishery, where _xx_ = calendar year, added across all brood years present, or _xx_ = brood year, added across calendar years. |
| NumComplBY | Number of brood years with all age classes present (to _MaxAge_)_._ |
| _NV__f,xx,._ | Total population vulnerable to fishery, where _xx_ = calendar year, added across all brood years present, or _xx_ = brood year, added across calendar years |
| _OcnEXR__by,a_ | Age specific exploitation rate |
| _PEsc__y,t_ | Proportion of escapement occurring in time period. |
| _PSC__f_ | Pacific Salmon Commission catch ceiling for fishery |
| _PNV__f,y,a_ | Proportion of Cohort that is Not Vulnerable. |
| PR | Proportion of a cohort that can be legally retained (determined by size limit restrictions) |
| _PV__f,y,a_ | Proportion of Cohort that is vulnerable. |
| QS | Instantaneous rate of fishing mortality by a sport fishery |
| QSM | Instantaneous rate of fishing mortality of Marked fish by a sport fishery during a mark selective fishery |
| QSU | Instantaneous rate of fishing mortality of Unmarked fish by a sport fishery during a mark selective fishery |
| QT | Instantaneous rate of fishing mortality by a troll fishery |
| QTU | Instantaneous rate of fishing mortality of Unmarked fish by a troll fishery during a mark selective fishery |
| QTT | Instantaneous rate of fishing mortality during non retention periods. |
| _RatioEXR__f,y,a_ | Exploitation rate index, calculated by ratio method |
| _RE__f,y,._ | Total Encounters of Legal-Sized fish during CNR fishery_._ |
| RetEffort | Effort observed during periods when Chinook retention is permitted |
| _RL__f,by,a_ | Mortality pf Legal-Sized fish during CNR fishery |
| _RL__f,y,._ | Total Mortalities of Legal-Sized fish during CNR fishery (summed across brood years)_._ |
| _rmL__f_ | Release Mortality Rate for Large fish |
| _rmS__f_ | Release Mortality Rate for Small fish |
| _RS__f,by,a_ | Mortality of Sub-legal sized fish during CNR fishery |
| _RS__f,y,._ | Total Sub-legal Mortalities during CNR fishery (summed across brood years)_._ |
| RT | Ratio between specified catch ceiling and the catch that would have been expected had the fishery been exerting base period exploitation rates used for the CTC Model |
| _SAVEXR__f,y,a_ | Simple Average exploitation rate index |
| _SeaCNR__f,y_ | Season Length (days) of CNR fishery |
| _SeaL__f,y_ | Season Length in time of Retention fishery |
| _SelLCNR__f_ | Length Selectivity of CNR fishery, relative to Retention fishery. |
| _SelSLCNR__f_ | Sub-legal Selectivity in CNR fishery, relative to Retention fishery |
| _Shak__f,by,a_ | Shaker mortality |
| _Shak__f,xx,._ | total shaker mortality for all ages, where _xx_ = calendar year, added across all brood years present, or _xx_ = brood year, added across calendar years_._ |
| _ShakPop__f,by,a_ | Population size for shaker calculations (varies according to terminal status of fishery and complete status of brood year |
| _SLCNR__f,y_ | Encounters of sublegal-sized fish during CNR (includes selectivity).  Externally provided by agency. |
| _SML__f_ | Shaker Mortality Rate for Legal-Sized fish. |
| _SMS__f_ | Shaker Mortality Rate for Sub-Legal Sized fish. |
| _SpEsc__by,a,t_ | Spawning Escapement |
| _StrayRate__y_ | Stray Rate from location where CWT recoveries for escapements are reported |
| _SurvRte__a_ | Survival Rate (1 - Natural Mortality Rate)_._ |
| TotalMortM | Total Mortality of Marked fish during a mark selective fishery |
| TotalMortU | Total Mortality of Unmarked fish during a mark selective fishery |
| _TotCat__f,by,a_ | Total Mortalities. |
| _TotMatCat__by,a_ | Total Mature (terminal) catch. |
| _TotOcnCat__by,a_ | Total PreTerminal catch |
| ZM | Total instantaneous rate of fishing mortality of Marked fish during a mark selective fishery |
| ZT | Total instantaneous rate of fishing mortality |
| ZTT | Total instantaneous rate of fishing mortality during non retention |
| ZU | Total instantaneous rate of fishing mortality for Unmarked fish during a mark selective fishery |
|   |   |
|   |   |

# APPENDIX C
**Estimation of Proportion Non-Vulnerable**

Incidental fishing mortalities result from discards caused by regulations (e.g., size limits or the operation of the gear, such as gillnet mesh size restrictions).  In order to estimate these mortalities, a given population is divided into two parts: (1) the proportion that is not vulnerable to the fishery (PNV); and (2) the proportion that is vulnerable to the fishery (PV=1-PNV).  PNVs are based on estimates of the proportions of a population that lie above or below a specified minimum size.

**Previous Method:**

When the CA methods were developed, the data available to estimate PNVs was very limited and was insufficient to estimate for each individual stock.  Therefore, PNV factors were computed for individual fisheries, based on observed length distributions of CWT recoveries.[^10]

    The lengths reported with CWT recoveries represented a large and readily available data set that can be identified accurately as to age of fish and catch location.  A description of the procedure used to estimate the PVs by age follows:

Due to the absence of sufficient, direct observational data on the size distribution of fish encountered by a particular fishery, age-length data from CWT tag recoveries were examined from troll and seine fisheries from Canada and some U.S. fisheries.  Generally, size distributions of fish of a given age were derived by combining data from the troll and seine fisheries.  Troll CWT data were used because they accounted for the largest amount of data even though the length measurements would be biased because of the effect of minimum size limits.  Seine data were preferred because they are potentially the least size-selective of the fisheries.  Seine data from Canadian fisheries appeared to be lacking representative fish in the larger size classes while the troll data lacked fish in the smaller size classes (due to size limits).  The two data sets were pooled to give large combined data sets for each region (e.g., West Coast Vancouver Island).  Only the Alaska seine data were used to estimate the size distribution of Chinook salmon encountered by the Alaska troll fishery.  Gillnet recoveries were not used because of size selectivity and because of morphological changes as fish moved closer to spawning areas.  Sport recoveries were not useful since most returns are from voluntary sources without sampling and consistent measuring procedures.

Although no statistical tests were performed, visual inspection seemed to indicate that year-to-year variability was less than area-to-area variability (no attempt was made to evaluate data for individual stocks for potential differences).  Therefore; data across years were combined to produce specific age-size distributions for particular groups of fisheries.

Aging was performed on a calendar year basis (recovery year – brood year).

Empirical cumulative distribution functions by fish length were generated and incorporated into a _Lotus 1-2-3_ spreadsheet PVCalc.WKS.  PNVs were computed as the proportion of the fish below a specified length using a table lookup function.

Age 2 vulnerability.  In addition to the CWT length at recovery data, a small number of agency monitoring reports provided estimates of the number of fish below the legal size limit.  The PSC Chinook Model estimates the encounter rates (non-retained/retained) by model fisheries based on PNV values specified as inputs.  The Model-generated estimates of encounter rates were compared to field data where available.  The PNV&#39;s for age 2 fish were adjusted iteratively until they corresponded as closely as possible to the observed data.  This is done by specifying an age 2 vulnerability factor in _PVCalc.WKS._

**Proposed Method (in development)**

The previous method has the effect of assuming that fish of a given age have the same size distribution for all stocks, and that fish of a given stock have different size distributions, depending on the fisheries in which they are caught.  Neither assumption is likely to be correct.  Different stocks are unlikely to grow at the same rates due to potential differences in biological characteristics such as timing of maturity and maturation.

More than two decades have passed since the original PNV procedure was developed.  There are several methods that could be employed to estimate stock-specific growth.  Some methods have been developed based on prorating observed length of fish based on periodic deposits of bony tissue (such as scales or otoliths) cf Ricker 1992.[^11]

    This approach has a major advantage over a CWT recovery in that information on growth rates during the entire lifetime can be obtained from any fish, regardless of whether or not it contains a CWT.  These methods are based on the presumption that growth in length is directly related to the distance from the focus (centroid of the circular deposits).  Data to support this presumption for wild fish from Pacific salmon stocks are sparse, but information from penned reared Atlantic salmon suggest that the method works reasonably well.  There are difficulties, however, in accurately reading scales (the small size of salmon scales exacerbates measurement errors), inconsistent annual age checks, interpreting the area of plus growth (the region of the scale or otolith that represents the last year of life), with scale resorbtion in terminal fisheries and escapements, and in obtaining sufficient data to characterize stock-specific growth patterns from individual growth histories.

CWT length measurement data continues to hold the most promise for estimation of growth rates and parameters to characterize size distributions by age.  The data set of CWT recoveries with reported length measurements is now much larger than it was during the early 1980s when the original size distributions were developed.  These data are to be processed through the following procedures:

1. For CWT recoveries with observed length measurements, expand observed recoveries for catch sampling rates (for recoveries with no expansion factor, use an expansion factor =1).

1. Stratify recoveries by run type and release stage.

1. Compute ocean age of fish, assuming that recoveries by marine fisheries are aged in the following manner (recoveries inn freshwater fisheries and escapements are not aged in this manner):

Fall run – October 1

Spring run  July 1

1. Compute ocean age of fish since date of last release, as reported in the CWT release record.

1. Combine recoveries for all brood years for hook and line fisheries and employ techniques to estimate the parameters of the Normal Distribution (assumed form for the underlying ocean age-length frequency distributions) using techniques to compensate for left-censored data[^12]  (due to minimum size limits).  Do this for time strata used in the CTC Model.

1. Combine recovery data for net fisheries (converting lengths in freshwater areas to ocean lengths based on Pahlke 1989[^13]) and estimate the Normal Distribution that best characterizes the ocean age-length distribution using maximum likelihood methods.

1. Perform a statistical test to determine of the cumulative distribution functions of the hook and line and net fisheries differ.  If not, combine data and re-estimate the parameters of the Normal Distribution.  If so, then use distribution for hook and line fisheries for pre-terminal fisheries and distribution of net fisheries for terminal fisheries.

1. Perform statistical tests to determine if there are significant stock-specific differences.  If not, combine data and generate growth functions.

1. Fit seasonal oscillating growth curve through the data to estimate mean length by ocean age and calendar period corresponding to stratification used for CTC Model.  Estimate variances (may be size dependent).

# References 
[^1]
 Also known as virtual population analysis.  Hilborn, R., and C. J. Walters. 1992. Quantitative fisheries stock assessment: Choice, dynamics, and uncertainty. Chapman and Hall, New York, 570 p.

[^2]
 Argue, A.W., R, Hilborn, R.M. Peterman, M.J. Staley, &amp; C.J. Walters. 1983.  Strait of Georgia Chinook and coho fishery. Can Bull. Fish, Aquat. Sci. 211: 91p.

[^3]
 Incomplete Broods.  CA is normally performed when all data for a given cohort are available.  However, because of the need to generate timely information on fishery impacts, CA is sometimes performed before all data for a complete brood are available.  In this case, average maturity rates are used to estimate the size of the cohort remaining after the last year of data encompassed in the CA.  If the CA were to be performed on incomplete data, cohort sizes would be underestimated and fishery exploitation rates would be biased high as a result. See Chapter 5.

[^4]
 The term &quot;exploitation rate&quot; refers to the proportion of an entire cohort that is killed by a fishery, i.e., it assumes that all the fish in a cohort are available to every preterminal fishery.

[^5]
 The term &quot;harvest rate&quot; refers to the proportion of the mature component of a cohort (after accounting for impacts of preterminal fisheries) that is killed by a fishery.  Harvest rates assume that all mature fish are available to every terminal fishery.

[^6]
 CTC Anaytical Workgroup.  &quot;Weighting&quot;.  Technical note dated May 7, 1987.  Unpublished.

[^7]
 Ricker, W.E. 1976.  Review of the rate of growth and mortality of Pacific salmon in salt water, and noncatch mortality caused by fishing.  J. Fish. Res. Bd. Can 33:1483-1524.

[^8]
 _Investigations of Methods to Estimate Mortalities of Unmarked Salmon in Mark-Selective Fisheries through the use of Double Index Tag Groups._ February, 2002. SFEC(02)-1.

[^9]
 Memo from G. Morishima to Paul Starr, Howard Schaller, John E. Clark, and Jim Scott, dated Sept 6, 1990, re: Problem with Brood Exploitation Rate Calculations.

[^10]
 CTC AWG notes.  Circa 1988.  &quot;Analysis of CWT Length Data:  Estimation of Length Frequencies of Legal and Sublegal Chinook Salmon By Stock.  Unpublished.

[^11]
 Ricker, W.E. 1992.  &quot;Back-Calculation of fish lengths based on proportionality between scale and length increments.  Can.J.Fish.Aquat.Sci. 49:1018-1026.

[^12]


[^13]
  Pahlke, K. 1989.  ADFG Research Bulletin 89-02.  &quot;Length Conversion Equations for Sockeye, Chinook, Chum, and Coho Salmon in Southeast Alaska&quot;.  Conversion to Mid-Eye to Hypural Plate (MEH) length from various length measurements of Chinook nearing spawning (mm):

Mid-Eye-Fork (MEF):  0.907\*MEF - 21.874

Post Orbit to Hypural Plate (POH): 0.994\*POH + 12.643

Snout to Fork (SNF):  0.765\*SNF + 19.497

Total Length (TOT):  0.762\*TOT + 0.524

Conversion to Total Ocean Length from Mid-Eye to Hypural Plate: 1.218\*MEH + 28.176