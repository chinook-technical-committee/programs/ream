REM this batch file can be run from windows shell by typeing its name at a prompt.

REM this batch file repeatedly calls R in batch mode, each instance is based on a single stock being run through ream. the output to R console is written to .Rout file, which is copied to a stock specific folder

"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='ATN'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/ATN/demo_execute_ream.Rout


"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='BQR'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/BQR/demo_execute_ream.Rout


"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='CHI'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/CHI/demo_execute_ream.Rout


"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='COW'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/COW/demo_execute_ream.Rout


"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='DOM'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/DOM/demo_execute_ream.Rout


"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='HAR'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/HAR/demo_execute_ream.Rout


"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='KLM'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/KLM/demo_execute_ream.Rout


"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='KLY'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/KLY/demo_execute_ream.Rout


"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='MSH'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/MSH/demo_execute_ream.Rout


"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='NIC'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/NIC/demo_execute_ream.Rout


"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='PHI'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/PHI/demo_execute_ream.Rout


"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='PPS'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/PPS/demo_execute_ream.Rout


"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='QUI'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/QUI/demo_execute_ream.Rout


"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='RBT'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/RBT/demo_execute_ream.Rout


"C:/PROGRA~1/R/R-41~1.1/bin/x64/R" CMD BATCH "--no-save --args Rexe.path='C:/PROGRA~1/R/R-41~1.1/bin/x64/R' stock.acronym='SHU'" demo_execute_ream.R
cp demo_execute_ream.Rout C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/exploitationRateAnalysis/reamTesting/reamout/SHU/demo_execute_ream.Rout


